<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Rtreport extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("report_modelrt"); // load Item moels
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 5){
				$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
				redirect('login');
		}
	}		
  public function report_list()
	{
		$select	 = array('DISTINCT(state_name),state_id');
		$where1 = array();
		$data['area'] = $this->report_modelrt->GetAllareaValues('area', $where1, $select);

		$select	 = array('nd_id','nd_code','fname','lname');
		$where2 = array('status'=> '1');
		$data['data'] = $this->report_modelrt->GetAllItemValues('ndistributor', $where2, $select);
		$fdate = '';
		$todate = '';
		$status = '';	
		$state_id = '';	
		$data['select']=array('status'=>$status,'state_id'=>$state_id,'fdate' => $fdate,'todate' => $todate);
		$this->renderView('Retailer/Report/report_list',$data);
	}


	public function report_list_filter()
	{
		$status = '';	
		$state_id = '';	
		$fdate = '';	
		$todate = '';
		$data = array();	
		if(!empty($_POST)){	
			$fromdate = (@$this->input->post('fdate')) ? $this->input->post('fdate').' 00:00:00' : '';
       	  	$toodate = (@$this->input->post('todate')) ? $this->input->post('todate').' 23:59:00' : '';

       	  	$fdate = date('Y-m-d H:i:s', strtotime($fromdate));
       	  	$todate = date('Y-m-d H:i:s', strtotime($toodate));
			$status = (@$this->input->post('status')) ? $this->input->post('status') : '';
			$state_id = (@$this->input->post('state_id')) ? $this->input->post('state_id') : '';
			$id =	 $this->session->userdata('id');
			$this->session->set_userdata('fdate',$fdate);
			$this->session->set_userdata('todate',$todate);
			$this->session->set_userdata('status',$status);
			$this->session->set_userdata('state_id',$state_id);
				if(@$_POST['submit']=='createxls')
       	 		{
					$data['data'] = $this->report_modelrt->get_report_list($status,$fdate, $todate, $limit = null, $start = null,$id);
					//echo '<pre>';print_r($data['data']); die; 
					$this->generate_report_excel($data['data'],$status);			
				}
			$config = array();
	        $config["base_url"] = site_url() . "/Rtreport/report_list_filter_sess";
	        $config["total_rows"] = $this->report_modelrt->get_report_count($status,$fdate, $todate,$id);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $data["links"] = $this->pagination->create_links();
	        $data['result'] = $result  = $this->report_modelrt->get_report_list($status,$fdate, $todate, $config["per_page"], $page,$id); 
		}	

			$data['select']=array('status'=>$status,'state_id'=>$state_id,'fdate' => $fdate,'todate' => $todate);
			$this->renderView('Retailer/Report/report_list',$data);
	}


	
	public function report_list_filter_sess()
	{
		
		$select	 = array('DISTINCT(state_name),state_id');
		$where1 = array();
		$data['area'] = $this->report_modelrt->GetAllareaValues('area', $where1, $select);

		$select	 = array('nd_id','nd_code','fname','lname');
		$where2 = array('status'=> '1');
		$data['data'] = $this->report_modelrt->GetAllItemValues('ndistributor', $where2, $select);

		
		$fdate = date('Y-m-d', strtotime($this->session->userdata('fdate')));
		$todate = date('Y-m-d', strtotime($this->session->userdata('todate')));
		$status = $this->session->userdata('status');
		$state_id = $this->session->userdata('state_id');
		$id =	 $this->session->userdata('id');
		if(@$_POST['submit']=='createxls')
		{
			$data['data'] = $this->report_modelrt->get_report_list($status,$fdate, $todate, $limit = null, $start = null,$id);
			$this->generate_report_excel($data['data'],$status);			
		}

		$config = array();
		$config["base_url"] = site_url() . "/Rtreport/report_list_filter_sess";
		$config["total_rows"] = $this->report_modelrt->get_report_count($status,$fdate, $todate,$id);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["links"] = $this->pagination->create_links();
		$data['result'] = $result  = $this->report_modelrt->get_report_list($status,$fdate, $todate,$config["per_page"], $page,$id); 	 
		$data['select']=array('status'=>$status,'state_id'=>$state_id,'fdate' => $fdate,'todate' => $todate);
		$this->renderView('Retailer/Report/report_list',$data);
	}

	

//generate to excel	
	public function generate_report_excel($param1,$status){
		// create file name
		$fileName = 'SalesReport'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		switch($status){
              case '1': 
                 $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sale To ND Date');
				 $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Company Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Company Firmname');
				 $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'ND Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ND FirmName');
				 $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'City');
				 $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'State');
				 $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Item Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'IMEI');
                break;
              case '2':
                 $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sale To MD Date');
				 $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ND Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ND Firmname');
				 $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'MD Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'MD FirmName');
				 $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'City');
				 $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'State');
				 $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Item Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'IMEI');
                break;
              case '3':
                 $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sale To RT Date');
				 $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ND Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ND Firmname');
				 $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'MD Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'MD FirmName');	
				 $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ASM Name');	
				 $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'TSM Name');	
				 $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'RT Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'RT FirmName');
				 $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'City');
				 $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'State');
				 $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Item Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'IMEI');
                break;
              case '4':
                 $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sale To Client Date');
				 $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ND Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ND Firmname');
				 $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'MD Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'MD FirmName');
				 $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ASM Name');	
				 $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'TSM Name');
				 $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'RT Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'RT FirmName');
				 $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Client Name');
				 $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Client Number');
				 $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'City');
				 $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'State');
				 $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Item Code');
				 $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'IMEI');
                break;
            }

		
				
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			switch($status){
              case '1': 
              case '2':
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, date('d-M-Y', strtotime($element['sales_date'])));
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,  $element['fromcode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['fromfirmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['tocode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['tofirmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['cityname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['state_name']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['item_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['imei']);
                break;
              case '3':
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, date('d-M-Y', strtotime($element['sales_date'])));
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,  $element['ndcode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount,  $element['ndfirmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount,  $element['fromcode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['fromfirmname']);
				if($element['atsmcode']){
					$asm_name = (@$element['atsmlevel'] == '0') ? $element['atsmfname'].' '.$element['atsmlname'] :$element['asmfname'].' '.$element['asmlname'];
				}
				else{
					$asm_name= 'TBA';
				}
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $asm_name);
				$tsm_name = (@$element['atsmlevel'] == '1') ? $element['atsmfname'].' '.$element['atsmlname'] : 'TBA';
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $tsm_name);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['tocode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['tofirmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['cityname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['state_name']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['item_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['imei']);
                break;
              case '4':
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, date('d-M-Y', strtotime($element['sales_date'])));
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,  $element['ndcode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount,  $element['ndfirmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount,  $element['dcode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount,  $element['dfirmname']);
				if($element['atsmcode']){
					$asm_name = (@$element['atsmlevel'] == '0') ? $element['atsmfname'].' '.$element['atsmlname'] :$element['asmfname'].' '.$element['asmlname'];
				}
				else{
					$asm_name= 'TBA';
				}
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $asm_name);
				$tsm_name = (@$element['atsmlevel'] == '1') ? $element['atsmfname'].' '.$element['atsmlname'] : 'TBA';
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $tsm_name);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount,  $element['fromcode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['fromfirmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['tocode']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['tofirmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['cityname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['state_name']);
				$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['item_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['imei']);
                break;
            }
			
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}	

	public function sale_list()
	{	
		
		$select	 = array('nd_id','nd_code','fname','lname');
		$where = array('status'=> '1');
		$pagedata['nd_list'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);

        $select	 = array('stnd_id','item_code','imei','nd_code','item_status','upload_date');
		$where = array();
		$pagedata['delete_link'] = 'Adminsales/delete_item';
		$status ='';
		$imei ='';
		$nd_id ='';
		if($this->session->userdata('imei') || $this->session->userdata('status')|| $this->session->userdata('nd_id')){
			$this->session->userdata('imei');
			$this->session->userdata('status');
			$this->session->userdata('nd_id');
		}
    
      //Pagination Start
		$config = array();
        $config["base_url"] = base_url() . "Adminsales/sale_list";
        $config["total_rows"] = $this->base_models->get_count('stnd_id','tbl_sales_to_nd', $where);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $pagedata["links"] = $this->pagination->create_links();
        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_nd', $where,'stnd_id',$config["per_page"], $page);     
       	//Pagination End

       	$imei = (@$imei) ? $imei : '';
       	$sstatus = (@$status) ? $status : null;
       	@strcmp($status,$sstatus);
       	$nd_id1 = (@$nd_id) ? $nd_id : null;
       	@strcmp($nd_id,$nd_id1);
        $pagedata['select']=array('status'=>$status,'imei'=>$imei,'nd_id'=>$nd_id);  
        $this->renderView('Admin/Sale/sale_list',$pagedata);
	}


	// with ci pagination in php
	public function sale_list_sess()
	{

		$select	 = array('nd_id','nd_code','fname','lname');
		$where = array('status'=> '1');
		$pagedata['nd_list'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);
       // print_r($_POST);
	    $select	 = array('stnd_id','item_code','imei','nd_code','item_status','upload_date');
		$where = array();
		$pagedata['delete_link'] = 'Adminsales/delete_item';
		$status ='';
		$imei ='';
		//Filter Process
	
       if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls')
       {
       	  	$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
       		$imei = trim($imei_no);

       	    $status = (@$this->input->post('status')!= null) ? $this->input->post('status') : '';
          
           $nd_id = (@$this->input->post('nd_id')!= null) ? $this->input->post('nd_id') : '';
           $array_items = $this->session->set_userdata(array("imei"=>$imei,"status"=>$status,"nd_id"=>$nd_id));
            if($imei !=''){
            	$filter =  array('imei'=> $imei);
            	$where = array_merge($where,$filter);	
            }  
            if($status !=''){
           		$filter =  array('item_status'=>$status);
            	$where = array_merge($where,$filter);	
            } 
             if($nd_id !=''){
           		$filter =  array('nd_id'=>$nd_id);
            	$where = array_merge($where,$filter);	
            } 
         
       }else{
				if($this->session->userdata('imei') != NULL){
				$imei = $this->session->userdata('imei'); 
				$filter =  array('imei'=> $imei);
				$where = array_merge($where,$filter);
				} 
				if($this->session->userdata('status') != NULL){
				$status = $this->session->userdata('status'); 
				$filter =  array('item_status'=>$status);
				$where = array_merge($where,$filter);
				}
				if($this->session->userdata('nd_id') != NULL){
				$nd_id = $this->session->userdata('nd_id'); 
				$filter =  array('nd_id'=>$nd_id);
				$where = array_merge($where,$filter);
				}
       	}
       		/*echo '<pre>';
		print_r($where); die();*/
       	 if(@$_POST['submit']=='createxls')
       	  {

       	  //	$select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei');
			//$where .= array('status'=> '1');
			$data['data'] = $this->item_sales->GetAllItemValues('tbl_sales_to_nd', $where, $select);
	     /* echo '<pre>';
	       print_r($data['data']);*/
			//Export xls
				$this->generate_sale_excel($data['data']);			
				

          }
		//End Filter Process
    
      //Pagination Start
		$config = array();
        $config["base_url"] = base_url() . "Adminsales/sale_list_sess";
        $config["total_rows"] = $this->base_models->get_count('stnd_id','tbl_sales_to_nd', $where);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $pagedata["links"] = $this->pagination->create_links();
        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_nd', $where,'stnd_id',$config["per_page"], $page);     
       	//Pagination End

       	$imei = (@$imei) ? $imei : '';

       	$sstatus = (@$status) ? $status : null;
        @strcmp($status,$sstatus);
        $nd_id1 = (@$nd_id) ? $nd_id : null;
       	@strcmp($nd_id,$nd_id1);
       @$pagedata['select']=array('status'=>$status,'imei'=>$imei,'nd_id'=>$nd_id);  
       $this->renderView('Admin/Sale/sale_list',$pagedata);
	}	



	public function edit_sale_item()
	{
		$select	 = array('nd_id','nd_code','fname','lname');
		$where = array('status'=> '1');
		$pagedata['nd_list'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);

		$id = base64_decode($_GET['id']); 
		$select	 = array('stnd_id','nd_id','upload_date','imei');
		$where = array('stnd_id'=> $id);
		$pagedata['data']=$this->item_sales->GetAllItemValues('tbl_sales_to_nd', $where, $select);
		$this->renderView('Admin/Sale/edit_sale_items',$pagedata);
	}


	public function update_sale_items()
	{
		if (isset($_POST)) 
		{

			$imei = trim($this->input->post('imei'));
			$nd_id = $this->input->post('nd_id');
			$datef 	= date("Y-m-d", strtotime($_POST['upload_date'])); 
			$time = date("h:i:s");
	   		$date= $datef.' '.$time;

			$item_id = $this->input->post('stnd_id');
			
			$select	 = array('nd_code');
			$where = array('nd_id'=> $nd_id);
			$arr_nd	=$this->item_sales->GetAllItemValues('ndistributor', $where, $select);
            $nd_code =$arr_nd[0]['nd_code'];    
			$update_array	=	array(
										'nd_id'  		=> $this->input->post('nd_id'),
										'nd_code'  		=> $nd_code,
										'upload_date'  		=> $date,
										'updated_on'	=> date("Y-m-d H:i:s")
									);
			$where_array	=	 array('imei'=>$imei);
            
            $this->base_models->update_records('tbl_sales_to_nd',$update_array,$where_array);
			
			$update_array1	=	array(
										'nd_id'  		=> $this->input->post('nd_id'),
										'nd_code'  		=> $nd_code,
										'nd_date'  		=> $date,
										'updated_on'	=> date("Y-m-d H:i:s")
									);
			$where_array1	=	 array('imei'=>$imei);


			if($this->base_models->update_records('tbl_item_sales',$update_array1,$where_array1) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
		}
			redirect(base_url('Adminsales/edit_sale_item/?id='.base64_encode($item_id)));	
	}	
			public function delete_sale_item()
			{
				 $imei = trim($_GET['id']); 
				$current_date = date("Y-m-d H:i:s");

				$update_array1 = array(
									'level_type'=>'0',
									'nd_id'=>'0',
									'nd_code'=>null,
									'nd_date'=>null,
									'updated_on'=>$current_date
									);
				$where_array1 = array('imei'=>$imei);
				$this->base_models->update_records('tbl_item_sales',$update_array1,$where_array1);

				$update_array = array(
									'item_status'=>'0',
									'updated_on'=>$current_date
									);
				$where_array = array('imei'=>$imei);
				if($this->base_models->update_records('tbl_items',$update_array,$where_array) == true){
					$this->base_models->delete_records('tbl_sales_to_nd',array('imei'=>$imei));//delete item from tbl item sale to nd
					   $this->session->set_flashdata('success','Item Deleted Successfully From Sale List');
				}else{
				  $this->session->set_flashdata('error','Item Not Deleted Successfully From Sale List');
				}
			redirect('Adminsales/sale_list/');
			}
   
    public function sales_detail_report()
	{
		$this->renderView('Admin/Report/sales_detail_report');
	}

	public function sales_detail_report_sess()
	{
		$imei = null;
		
		//Filter Process	
		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls'){
			$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
			$imei = trim($imei_no);
			$array_items = $this->session->set_userdata(array("imei"=>$imei));
		}else{
			if($this->session->userdata('imei') != NULL){
				$imei = $this->session->userdata('imei');
			}
		}
		 
		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->report_modelrt->get_all_stock($imei);
			$this->generate_stock_excel($data['data']);		
		}
		//End Filter Process
	
		
		$pagedata['results'] = $this->report_modelrt->get_all_stock($imei);  
		//Pagination End

		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Admin/Report/sales_detail_report',$pagedata);
	}

	//generate to excel	
	public function generate_stock_excel($param1){
		// create file name
		$fileName = 'SalesReport'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'From Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'From FirmName');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'To Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'ToFirmName');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'IMEI');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Item Code');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			if($element['level_type']=='1' || $element['level_type']=='2' ||$element['level_type']=='3'){
				$name ="Anuron";
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount,$name);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $name);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['nd_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['nd_firmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, date("d-M-y", strtotime($element['nd_date'])));
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['imei']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['item_code']);
			}
			$rowCount=3;
			if($element['level_type']=='2' ||$element['level_type']=='3'){
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount,$element['nd_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['nd_firmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['d_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['d_firmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, date("d-M-y", strtotime($element['d_date'])));
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['imei']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['item_code']);
			}
			$rowCount=4;
			if($element['level_type']=='3'){	
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount,$element['d_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['d_firmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['rt_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['rt_firmname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, date("d-M-y", strtotime($element['rt_date'])));
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['imei']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['item_code']);
			}
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}



    public function check_imei_exist()
	{
		$rs 			= $this->db->query("select imei from tbl_items where status='1'");
		$array 			= $rs->result_array();
		$imei_array 	= array_column($array, 'imei');	 // for converting in 1 array format
		$imei_array1 	= array_flip($imei_array);

		$imei 	        = $_POST['imei'];

		$imei_code 		= trim($imei); // remove spaces from both sides of string

			 if(array_key_exists($imei_code, $imei_array1))
			 {
				echo 'error';
			 }
			 else
			 {
			 	echo 'success';
			 }	
	}	 

	//---------- ND ------------//
	public function national_distributor()
	{		
		$select = array('nd_id','nd_code','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select date_time from recent_login_user where user_code = nd_code AND type = "1" Order by id desc limit 1) as last_login');
		$pagedata['results'] = $this->base_models->GetAllValues('ndistributor',null, $select);
		echo '<pre>';
		print_r($pagedata['results']);
		die();
		$pagedata['delete_link'] = 'Adminmaster/delete_nd';
		$this->renderView('Admin/Master/national_distributor',$pagedata);
	}
	
	
	
	public function insert_nd()
	{
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            if(!empty($_FILES['image']['name'])){
                $config['upload_path'] = 'uploads/admin/users/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
					$data = $this->upload->data();
				}else{
                    $imageerrors = $this->upload->display_errors();
					$this->form_validation->set_message('image', $imageerrors);					
                }
			}
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>md5($this->input->post('inputPassword')),
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						'profile_pic'=>$data['file_name'],
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('wwc_admin',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(base_url('admin/users'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('Admin/add-user');
	}
	
	public function edit_nd()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->get_users('',$id);
		$this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function update_nd()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('admin/users')); 
		}
		
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>$password,
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(base_url('admin/edit_user/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_nd()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- END ND ------------//
	
		
}
