<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Distributor extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 4){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	//-- Anuron ERP--//
	
	public function dashboard()
	{		
		redirect('Dmaster/add_rt');
		$this->renderView('Distributor/dashboard');
	}
	
	//END Anuron ERP--//			
}
