<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Dactivate extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("d_activate_models"); // load nd sales models 
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 4){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	// Start Singel IMEI Activation
	public function single_imei()
	{	
		$d_id =	 $this->session->userdata('id');
	
	
		$imei ='';	
		if($this->session->userdata('imei') ){
			$this->session->userdata('imei');
		}
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Dactivate/single_imei";
		$config["total_rows"] = $this->d_activate_models->get_single_imei_rt_cnt($d_id);

		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->d_activate_models->get_single_imei_rt($d_id,$config["per_page"], $page);     
		//print_r($pagedata["links"]);die;
		//Pagination End
			
		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Distributor/Activation/single_activation',$pagedata);
	}

	// with ci pagination in php
	public function single_imei_sess()
	{
	   $d_id =	 $this->session->userdata('id');
	
	
		$imei ='';	
		if($this->session->userdata('imei') ){
			$this->session->userdata('imei');
		}
		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls'){
			$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
			$imei = trim($imei_no);
		}
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Dactivate/single_imei_sess";
		$config["total_rows"] = $this->d_activate_models->get_single_imei_rt_cnt($d_id);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->d_activate_models->get_single_imei_rt($d_id,$config["per_page"], $page,$imei);     
		//print_r($pagedata["links"]);die;
		//Pagination End
			
		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Distributor/Activation/single_activation',$pagedata);
	}	

	public function updatesingleimei()
	{
		$current_date = date("Y-m-d H:i:s");
		$cilent_name = $this->input->post('cilent_name');
		$cilent_mob = $this->input->post('cilent_mob');
		$imei_code = trim($this->input->post('imei_code'));
		$d_id = $this->session->userdata('id');
		$d_code =	 $this->session->userdata('code');	
		$this->db->trans_begin();// transaction start
		
		//insert client
		$cldata  = array('d_id' => $d_id,
						'fname'   => $cilent_name,
						'contact'   => $cilent_mob,
						'inserted_on'=> date('Y-m-d h:i:s')
						);
		$this->db->insert("client",$cldata);
		$insert_id = $this->db->insert_id();
		//fetch client details
		$cl = $this->db->query("select c_id,c_code from client where c_id='$insert_id'");
		$client_array = $cl->result_array();
		$c_id = $client_array[0]['c_id'];
		$c_code = $client_array[0]['c_code'];

		//insert tbl_sales_to_c
		$rs         = $this->db->query("select item_id,item_code from tbl_items where imei='$imei_code'");
		$item_array = $rs->result_array();
		$item_id = $item_array[0]['item_id'];
		$item_code  = $item_array[0]['item_code'];
		$date =	date('Y-m-d h:i:s');
		$email	= false;
		// enable_date checker
		$where_enable_date = array('status !=' => '2','d_id' => $this->session->userdata('id'));
		$check_date = $this->base_models->GetSingleDetails('distributor', $where_enable_date, $select=array('enable_date','device_token')); // to check enable_date
		if($check_date->enable_date == 1){
			$date =	date('Y-m-d h:i:s',strtotime($check_date->device_token));
			$email	= true;
		}

		$data  = array('item_id'  	=> $item_id,
					'item_code	'   => $item_code,
					'imei'   		=> $imei_code,
					'c_id'   		=> $c_id,
					'c_code'    	=> $c_code,
					'upload_date'   => $date,
					'inserted_on'   => date('Y-m-d h:i:s')
					);
		$this->db->insert("tbl_sales_to_c",$data);
		
		// For Code Tracking imei
		$tis         = $this->db->query("select rt_id,is_id from tbl_item_sales where imei='$imei_code'");
		$tis_array = $tis->result_array();
		$tis_id 	= 	$tis_array[0]['is_id'];
		
		$arr_tracking = array(
				'imei'  		=> $imei_code,
				'ref_id'   		=> $c_id,
				'ref_code'   	=> $c_code,
				'tis_id'   		=> $tis_id,
				'level_type'   	=> '4',
			    'track_status'  => '1',
				'date'   		=> $date
			);
		$this->db->insert("tbl_item_tracking",$arr_tracking);

		//update tbl_item_sales
		$rt_id = $tis_array[0]['rt_id'];
		$rt = $this->db->query("select tsm_id from retailer where rt_id='$rt_id'");
		$rt_array = $rt->result_array();
		$tsm_id  	= 	$rt_array[0]['tsm_id'];
		$update_array	=	array(
			'level_type'  	=> '4',
			'c_id'      => $c_id,
			'c_code'	  => $c_code,
			'c_date'   => $date,
			'atsm_id'	=> $tsm_id,
			'updated_on'	=> date("Y-m-d H:i:s")
			);
		$where_array	=	array('imei'=> $imei_code);
		$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
		
		//update tbl_sales_to_rt
		$update_array = array(
						'item_status'=>'1',
						'updated_on'=>$current_date
						);
		$where_array = array('imei'=>$imei_code);
		$this->base_models->update_records('tbl_sales_to_rt',$update_array,$where_array);
		
		//send sms to client
		$msg = 'Testing';
		$cilent_mob = '9175917762';
		//$this->message_send($msg, $cilent_mob);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback(); //rollback
			$data['status'] = 'fail';
		}else{
			$this->db->trans_commit(); //commit
			// if($email == true){
			// 	$invoiceHtml = 'D code: '.$d_code.' | Backdate: '.date('d-M-Y',strtotime($check_date->device_token)).'<br> IMEI: '.$imei_code;
			// 	$this->push_email($invoiceHtml,$d_code,$clientEmail='mis@soham.co.in',$attachment=''); // send email
			// }
			$data['status'] = 'success';
		}
		echo json_encode($data);
			
	}
 // End Singel IMEI Activation
	// public function sendSMS(){
	// 	$msg = 'Testing';
	// 	$cilent_mob = '9175917762';
	// 	$this->message_send($msg, $cilent_mob);
	// }

   public function sale_to_rt()
	{
			$select	 = array('rt_id','rt_code','firmname');
			$where = array('status !=' => '2','d_id' => $this->session->userdata('id'));
			$data['data'] = $this->d_sales_models->GetAllItemValues('retailer', $where, $select);
		$this->renderView('Distributor/Sale/sale_items',$data);
	}

   public function activation()
	{
		$this->renderView('Distributor/Activation/activation_excel_form');
	}
	
	public function upload_activation_excel()
	{
		$d_id   =	 $this->session->userdata('id');
		$d_code =	 $this->session->userdata('code');
		if(empty($_FILES["acivation_excel"]["tmp_name"])){
			redirect(site_url('Dactivate/activation/'));
		}

		$this->load->library('excel');
		
		$rt = $this->db->query("select rt_code from retailer where d_id='$d_id'");
		$array_rt = $rt->result_array();
 		$rt_array = array_column($array_rt, 'rt_code');	 // for converting in 1 array format
		$rt_array1 =  array_flip($rt_array);
		$date =	date('Y-m-d h:i:s');
		$email	= false;
		// enable_date checker
		$where_enable_date = array('status !=' => '2','d_id' => $this->session->userdata('id'));
		$check_date = $this->base_models->GetSingleDetails('distributor', $where_enable_date, $select=array('enable_date','device_token')); // to check enable_date
		if($check_date->enable_date == 1){
			$date =	date('Y-m-d h:i:s',strtotime($check_date->device_token));
			$email	= true;
		}
				if (isset($_POST)) 
				{
					//print_r($_FILES); die;
				   $path = $_FILES["acivation_excel"]["tmp_name"];
				   $object = PHPExcel_IOFactory::load($path);
				   foreach($object->getWorksheetIterator() as $worksheet)
				   {
					   $highestRow = $worksheet->getHighestRow();
					   $highestColumn = $worksheet->getHighestColumn();
					  
					    for($row=2; $row<=$highestRow; $row++)
					    {
							$rt_code1 		= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
							$imei 		= $worksheet->getCellByColumnAndRow(1, $row)->getValue(); 
							$cname1 			= $worksheet->getCellByColumnAndRow(2, $row)->getValue();
							$cnum1 			= $worksheet->getCellByColumnAndRow(3, $row)->getValue();
							$rt_code	=	trim($rt_code1);
							if(array_key_exists($rt_code, $rt_array1))
								{
									$imei_code  =	trim($imei); // remove spaces from both sides of string
									$rt_code  =	trim($rt_code1); // remove spaces from both sides of string
									$cname  =	trim($cname1); // remove spaces from both sides of string
									$cnum  =	trim($cnum1); // remove spaces from both sides of string
									$length = strlen($imei_code);
										if($length >= '1'){
											$sql 		= $this->db->query("select imei from tbl_sales_to_rt where rt_code='$rt_code'");
											$array_item = $sql->result_array();
									 		$item_array = array_column($array_item, 'imei');	 // for converting in 1 array format
									        $item_array1=  array_flip($item_array);

											$rs = $this->db->query("select imei from tbl_sales_to_rt where item_status='1'");
											$array = $rs->result_array();
											$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
									        $imei_array1 =  array_flip($imei_array);

											if(array_key_exists($imei_code, $item_array1)) // for item is in rt list or not
											{
											   	if(array_key_exists($imei_code, $imei_array1))
												{
													$sold[] = array
														(
															
															'imei'   				=> $imei_code
																		
														);
												}else{

													//insert client
													$cldata  = array('d_id' => $d_id,
																	'fname'   => $cname,
																	'contact'   => $cnum,
																	'inserted_on'=> date('Y-m-d h:i:s')
																	);
													$this->db->insert("client",$cldata);
													$insert_id = $this->db->insert_id();

													//fetch client details
													$cl = $this->db->query("select c_id,c_code from client where c_id='$insert_id'");
													$client_array = $cl->result_array();
													$c_id = $client_array[0]['c_id'];
													$c_code = $client_array[0]['c_code'];
													
													$rs         = $this->db->query("select item_id,item_code from tbl_items where imei='$imei_code'");
													$item_array = $rs->result_array();
													$item_id 	= 	$item_array[0]['item_id'];
													$item_code  = 	$item_array[0]['item_code'];
													//$date 	 =	date('Y-m-d h:i:s');
													
													$data[]  = array
														(
															'item_id'  			=> $item_id,
															'item_code	'   	=> $item_code,
															'imei'   			=> $imei_code,
															'c_id'   			=> $c_id,
															'c_code'    		=> $c_code,
															'upload_date'   	=> $date,
															'inserted_on'   	=> date('Y-m-d h:i:s')
														);
															
													//send sms to client
													$msg = 'Testing';
													$cnum = '9175917762';
													//$this->message_send($msg, $cnum);	 
					
												}
											}else{
													$item_not_in_rt_list[] = array
														(
															
															'imei' => $imei_code
																		
														);
												 }	
										}else{
												$invalid_imei_length[] = array
														(
															
															'imei' => $imei_code
																		
														);
											 }	
												   
								}else{
								
										$invalid_rt[] = array
														(
															
															'imei' => $imei
																		
														);
									}
						}
					}
					$this->db->trans_begin(); //trans start
					if(isset($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['imei']!='')
								 {	 
								 	$this->db->insert("tbl_sales_to_c",$row);
								 
									// For Code Tracking imei
									$imei_code = $row['imei'];
									$tis         = $this->db->query("select rt_id,is_id from tbl_item_sales where imei='$imei_code'");
									$tis_array = $tis->result_array();
									$tis_id 	= 	$tis_array[0]['is_id'];
									
									$arr_tracking = array(
											'imei'  		=> $imei_code,
											'ref_id'   		=> $row['c_id'],
											'ref_code'   	=> $row['c_code'],
											'tis_id'   		=> $tis_id,
											'level_type'   	=> '4',
											'track_status'  => '1',
											'date'   		=>$date
										);
									$this->db->insert("tbl_item_tracking",$arr_tracking);

									//update tbl_item_sales
									$rt_id = $tis_array[0]['rt_id'];
									$rt = $this->db->query("select tsm_id from retailer where rt_id='$rt_id'");
									$rt_array = $rt->result_array();
									$tsm_id  	= 	$rt_array[0]['tsm_id'];
								 	$update_array	=	array(
										'level_type'  		=> '4',
										'c_id'   		    => $row['c_id'],
										'c_code'	   		=> $row['c_code'],
										'c_date'   			=> $row['upload_date'],
										'atsm_id'   		=> $tsm_id,
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$where_array	=	 array('imei'=> $row['imei']);
									$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);

									//update tbl_sales_to_rt
									$update_array1	=	array(
										'item_status'  		=> '1',
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$this->base_models->update_records('tbl_sales_to_rt',$update_array1,$where_array);	
								 }
							}	
					}	

					if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    $this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					    //  $this->session->set_flashdata('success','Query run successfully');
					}
						if(isset($data))
						{    	
							if($email == true){
								$invoiceHtml = 'D code: '.$d_code.' | Backdate: '.date('d-M-Y',strtotime($check_date->device_token)).'<br> IMEI: '.implode(', ',array_column($data,'imei'));
								$this->push_email($invoiceHtml,$d_code,$clientEmail='mis@soham.co.in',$attachment=''); // send email
							}
							    $page_data['acceptedprodct']	=	$data;
							
						}

						if(isset($sold))
						{    	
							    $page_data['sold']	=	$sold;
							
						}
						if(isset($item_not_in_rt_list))
						{    	
							    $page_data['itemnotinlist']	=	$item_not_in_rt_list;
							
						}
						if(isset($invalid_imei_length))
						{    	
							    $page_data['invalidimei']	=	$invalid_imei_length;
							
						}
					 	if(isset($invalid_rt))
						{    	
							    $page_data['invalid_rt']	=	$invalid_rt;
							
						}
						/*echo '<pre>';
						print_r($page_data); die;*/
					 	$this->renderView('Distributor/Activation/activation_excel_form',$page_data);
				}
	}
}
