<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminsales extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("item_sales"); // load Item moels
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function faulty_return_list_of_admin()
	{	
		$select	 = array('if_id','imei','reason','d_date','nd_date','admin_status','nd_code','admin_date','(select image_url from images where type= 7 and ref_code = tbl_item_faulty.if_id limit 1) as image_url');
		$where = array('admin_date !=' => 'null','level_type' => '0');
		//$pagedata['delete_link'] = 'Adminitems/delete_item';
	
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminsales/faulty_return_list_of_admin";
		$config["total_rows"] = $this->base_models->get_count('if_id','tbl_item_faulty', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination_data($select,'tbl_item_faulty',$where,$orderby = 'if_id',$config["per_page"], $page);      
		//Pagination End

		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Admin/Sale/faulty_return_list_of_admin',$pagedata);
	}

	public function faulty_return_list()
	{	
		
		$level_type	=	'0';
	
		$imei ='';	
		if($this->session->userdata('imei') ){
			$this->session->userdata('imei');
		}
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminsales/faulty_return_list";
		$config["total_rows"] = $this->item_sales->get_faulty_list_cnt($level_type);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] =  $this->item_sales->get_faulty_list($level_type,$config["per_page"], $page);     
		//echo'<pre>';print_r($pagedata["results"]);die;
		//Pagination End
			
		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Admin/Sale/faulty_list',$pagedata);
	}
     
    public function updatefaultyimeiadmin()
	{	
		
		$imei 		= $_POST['imei'];
		$reason 	= $_POST['reason'];
		$admin_status 	= $_POST['admin_status'];
		
		
		$update_array[]   = array
			(
				
				'imei'   			=> $imei,
				'admin_status'   	=> $admin_status,
				'admin_date'		=> date("Y-m-d H:i:s")
			);
		
		$this->db->trans_begin(); //trans start
		if(!empty($update_array))
		{
			foreach ($update_array as $row)
			{	 
			    if($row['imei']!='')
				{	
					$where_array = array('imei'=>$row['imei']);
					$this->base_models->update_records('tbl_item_faulty',$row,$where_array);
				}
			}	
			
		}

		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback(); //rolback
		   	$data['status'] = 'fail';
		}else{
		    $this->db->trans_commit(); //commit
		  $data['status'] = 'success';
		}	

		echo json_encode($data);
				
	}

	// Sale Return By Excel
	public function sale_return_by_excel()
	{
		$this->renderView('Admin/Sale/sale_return_by_excel');
	}
	public function upload_sale_return_by_excel()
	{
		$admin_id =	 $this->session->userdata('id');	
		$this->load->library('excel');
		
		$nd = $this->db->query("select nd_code from ndistributor where admin_id='$admin_id'");
		$array_nd = $nd->result_array();
 		$nd_array = array_column($array_nd, 'nd_code');	 // for converting in 1 array format
        $nd_array1 =  array_flip($nd_array);
				
				if (isset($_POST)) 
				{
					//print_r($_FILES); die;
				   $path = $_FILES["sale_return"]["tmp_name"];
				   $object = PHPExcel_IOFactory::load($path);
				   foreach($object->getWorksheetIterator() as $worksheet)
				   {
					   $highestRow = $worksheet->getHighestRow();
					   $highestColumn = $worksheet->getHighestColumn();
					  
					    for($row=2; $row<=$highestRow; $row++)
					    {
							$nd_code1 		= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
							$imei 			= $worksheet->getCellByColumnAndRow(1, $row)->getValue();
							$nd_code		= trim($nd_code1);
							if(array_key_exists($nd_code, $nd_array1))
								{
									$imei_code  =	trim($imei); // remove spaces from both sides of string
									$length = strlen($imei_code);
										if($length > '10'){
											 // array For product is in the listof d  or not 
											$rs = $this->db->query("select imei from tbl_sales_to_nd where nd_code='$nd_code'");
											$array = $rs->result_array();
											$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
									        $imei_array1 =  array_flip($imei_array);
											
											// array for product sold 
									        $rsold = $this->db->query("select imei from tbl_sales_to_nd where item_status='1'");
											$arraysold = $rsold->result_array();
											$sold_imei_array = array_column($arraysold, 'imei');	 // for converting in 1 array format
									        $sold_imei_array1 =  array_flip($sold_imei_array);
												
												if(array_key_exists($imei_code, $imei_array1)) // for item is in d list or not
												{

												   	if(array_key_exists($imei_code, $sold_imei_array1))
													{
														$sold[] = array
															(
																
																'imei'   				=> $imei_code
																			
															);
													}else{
															$data[]  = array
																(
																	'imei'   			=> $imei_code
																);
														  }
												}else{
														$item_not_in_nd_list[] = array
															(
																
																'imei' => $imei_code
																			
															);
													 }	
											}else{
													$invalid_imei_length[] = array
															(
																
																'imei' => $imei_code
																			
															);
												 }	
													   
									}else{
											$invalid_nd[] = array
															(
																
																'imei' => $imei
																			
															);
										}
					}
		}
		
					$this->db->trans_begin(); //trans start
					if(!empty($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['imei']!='')
								 {	
								 	if($this->base_models->delete_records('tbl_sales_to_nd',array('imei'=>$row['imei'])) == true){
									 	// For Code return Tracking imei
								 	$imei  	= $row['imei'];	
								 	$tis         = $this->db->query("select is_id from tbl_item_sales where imei='$imei'");
									$tis_array = $tis->result_array();
									$tis_id 	= 	$tis_array[0]['is_id'];
									$ref_id 	= 	'1';
									$ref_code 	= 	'admin';
									
									$arr_tracking = array(
											'imei'  		=> $row['imei'],
											'ref_id'   		=> $ref_id,
											'ref_code'   	=> $ref_code,
											'tis_id'   		=> $tis_id,
											'level_type'   	=> '0',
										    'track_status'  => '2',
											'date'   		=> date('Y-m-d h:i:s')
										);
									$this->db->insert("tbl_item_tracking",$arr_tracking);
									 	$update_array	=	array(
											'level_type'  	=> '0',
											'nd_id'   		=> '0',
											'nd_code'   	=> null,
											'nd_state_id'   => null,
											'nd_city_id'   	=> null,
											'nd_date'   	=> null,
											'updated_on'	=> date("Y-m-d H:i:s")
										);
										$where_array	=	 array('imei'=> $row['imei']);
										$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
										$update_array1	=	array(
											'item_status'  		=> '0',
											'updated_on'		=> date("Y-m-d H:i:s")
										);
										$this->base_models->update_records('tbl_items',$update_array1,$where_array);	
									}						 
								 }
							}	
					}

					if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    $this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					    //  $this->session->set_flashdata('success','Query run successfully');
					}	

						if(isset($data))
						{    	
							    $page_data['acceptedprodct']	=	$data;
							
						}

						if(isset($sold))
						{    	
							    $page_data['sold']	=	$sold;
							
						}
						if(isset($item_not_in_nd_list))
						{    	
							    $page_data['itemnotinlist']	=	$item_not_in_nd_list;
							
						}
						if(isset($invalid_imei_length))
						{    	
							    $page_data['invalidimei']	=	$invalid_imei_length;
							
						}
					 	if(isset($invalid_nd))
						{    	
							    $page_data['invalid_nd']	=	$invalid_nd;
							
						}
						/*echo '<pre>';
						print_r($page_data); die;*/
					 	$this->renderView('Admin/Sale/sale_return_by_excel',$page_data);
				}
				
	}
	//End Sale Return By Excel


		public function sale_return()
	{
		$select	 = array('nd_id','nd_code','fname','lname','firmname');
		$where = array('status'=> '1');
		$data['data'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);
		$this->renderView('Admin/Sale/sale_return',$data);
	}
	public function return_sale_items()
	{	
		
		$nd_id 	= $_POST['nd_id'];
		$all_imei 	= $_POST['imei'];
		$arr_imei 	= preg_split('/\r\n|[\r\n]/', $all_imei);
		
		//$nd_id =$this->session->userdata('id');
		      // array For product is in the listof d  or not 
				$nd = $this->db->query("select imei from tbl_sales_to_nd where nd_id='$nd_id'");
				$array = $nd->result_array();
				$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
		        $imei_array1 =  array_flip($imei_array);
				
				// array for product sold 
		        $rsold = $this->db->query("select imei from tbl_sales_to_nd where item_status='1'");
				$arraysold = $rsold->result_array();
				$sold_imei_array = array_column($arraysold, 'imei');	 // for converting in 1 array format
		        $sold_imei_array1 =  array_flip($sold_imei_array);
		           

				if (!empty($arr_imei)) 
				{
					$i=0;
				  foreach($arr_imei as $row)
				   {
					    $imei 	     = $arr_imei[$i]; 
						$imei_code  =	trim($imei); // remove spaces from both sides of string
						
						   if(array_key_exists($imei_code, $imei_array1))
							{
								if(array_key_exists($imei_code, $sold_imei_array1))
								{
									$sold[] = array
									    (
											'imei' => $imei_code
										);
								}
								else
								{
									$data[]  = array
										(
											'imei'   			=> $imei_code,
										);
								}
							}
							else
							{
								$notinlist[] = array
									(
										'imei'=> $imei_code
									);
							}		  
						$i++;
					}
					
					$this->db->trans_begin(); //trans start
					if(!empty($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['imei']!='')
								 {	
								 	if($this->base_models->delete_records('tbl_sales_to_nd',array('imei'=>$row['imei'])) == true){
									 	
									 	// For Code return Tracking imei
								 	$imei  	= $row['imei'];	
								 	$tis         = $this->db->query("select is_id from tbl_item_sales where imei='$imei'");
									$tis_array = $tis->result_array();
									$tis_id 	= 	$tis_array[0]['is_id'];
									$ref_id 	= 	'1';
									$ref_code 	= 	'admin';
									
									$arr_tracking = array(
											'imei'  		=> $row['imei'],
											'ref_id'   		=> $ref_id,
											'ref_code'   	=> $ref_code,
											'tis_id'   		=> $tis_id,
											'level_type'   	=> '0',
										    'track_status'  => '2',
											'date'   		=> date('Y-m-d h:i:s')
										);
									$this->db->insert("tbl_item_tracking",$arr_tracking);
									 	$update_array	=	array(
											'level_type'  	=> '0',
											'nd_id'   		=> '0',
											'nd_code'   	=> null,
											'nd_state_id'   => null,
											'nd_city_id'   	=> null,
											'nd_date'   	=> null,
											'updated_on'	=> date("Y-m-d H:i:s")
										);
										$where_array	=	 array('imei'=> $row['imei']);
										$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
										$update_array1	=	array(
											'item_status'  		=> '0',
											'updated_on'		=> date("Y-m-d H:i:s")
										);
										$this->base_models->update_records('tbl_items',$update_array1,$where_array);	
									}						 
								 }
							}	
					}

					if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    $this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					    //  $this->session->set_flashdata('success','Query run successfully');
					}	

				

						
					if(!empty($data))
					{    	
						$page_data['acceptedprodct']	=	$data;
					}

					if(!empty($notinlist))
					{    	
	    				$page_data['rejectedprodct']	=	$notinlist;
					}

					if(!empty($sold))
					{    	
					    $page_data['soldprodct']		=	$sold;
					}
				 	
				 	$select	 = array('nd_id','nd_code','fname','lname','firmname');
					$where = array('status'=> '1');
					$page_data['select']=array('nd_id'=>$nd_id); 
					$page_data['data'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);
					
				 	$this->renderView('Admin/Sale/sale_return',$page_data);
				}
				
		}


   public function sale_items()
	{
			$select	 = array('nd_id','nd_code','fname','lname','firmname');
			$where = array('status'=> '1');
			$data['data'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);
		$this->renderView('Admin/Sale/sale_items',$data);
	}


	

	public function insert_sale_items()
	{
		
		$nd_id 	= $_POST['nd_id'];
		$time = date("h:i:s");
	   	$datef 		= date("Y-m-d", strtotime($_POST['date'])); 
	    $date= $datef.' '.$time;
		$all_imei 	= $_POST['imei'];
		$arr_imei 	= preg_split('/\r\n|[\r\n]/', $all_imei);
		
		      // array For product is in the list or not 
				$rs = $this->db->query("select imei from tbl_items where status='1'");
					$array = $rs->result_array();
					
				$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
		            $imei_array1 =  array_flip($imei_array);
				
				// array for product sold or not
		         $rsold = $this->db->query("select imei from tbl_sales_to_nd");
				 $arraysold = $rsold->result_array();
					
				$sold_imei_array = array_column($arraysold, 'imei');	 // for converting in 1 array format
		        $sold_imei_array1 =  array_flip($sold_imei_array);
		           

				if (!empty($arr_imei)) 
				{
					$i=0;
				  foreach($arr_imei as $row)
				   {
					    $imei 	     = $arr_imei[$i]; 
						 $imei_code  =	trim($imei); // remove spaces from both sides of string
						
						   if(array_key_exists($imei_code, $imei_array1))
							{
								if(array_key_exists($imei_code, $sold_imei_array1))
								{
									$sold[] = array
									    (
											'imei' => $imei_code
										);
								}
								else
								{
									$rs         = $this->db->query("select item_id,item_code from tbl_items where imei='$imei_code'");
									$item_array = $rs->result_array();
									$item_id 	= 	$item_array[0]['item_id'];
									$item_code  = 	$item_array[0]['item_code'];

									$nd = $this->db->query("select nd_code,state,city from ndistributor where nd_id='$nd_id'");
									$nd_array = $nd->result_array();
									$nd_code  = 	$nd_array[0]['nd_code'];
									$state  = 	$nd_array[0]['state'];
									$city  	= 	$nd_array[0]['city'];
									$data[] = array
										(
											'item_id'  			=> $item_id,
											'item_code	'   	=> $item_code,
											'imei'   			=> $imei_code,
											'nd_id'   			=> $nd_id,
											'nd_code'   		=> $nd_code,
											'upload_date'   	=> $date,
											'inserted_on'   	=> date('Y-m-d h:i:s')
										);
								}
							}
							else
							{
								$duplicate[] = array
									(
										'imei'=> $imei_code
									);
							}		  
						$i++;
					}
					
					$this->db->trans_begin(); //trans start
					if(!empty($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['nd_id']!='')
								 {	 
								 	$this->db->insert("tbl_sales_to_nd",$row);
								 	// For Code Tracking imei
								 	$imei  	= $row['imei'];	
								 	$tis         = $this->db->query("select is_id from tbl_item_sales where imei='$imei'");
									$tis_array = $tis->result_array();
									$tis_id 	= 	$tis_array[0]['is_id'];
									
									$arr_tracking = array(
											'imei'  		=> $row['imei'],
											'ref_id'   		=> $row['nd_id'],
											'ref_code'   	=> $row['nd_code'],
											'tis_id'   		=> $tis_id,
											'level_type'   	=> '1',
										    'track_status'  => '1',
											'date'   		=> $date
										);
									$this->db->insert("tbl_item_tracking",$arr_tracking);
									// End Code Tracking imei
								 	$nd_id = $row['nd_id'];
								 	$nd = $this->db->query("select state,city from ndistributor where nd_id='$nd_id'");
									$nd_array = $nd->result_array();
									$state  = 	$nd_array[0]['state'];
									$city  	= 	$nd_array[0]['city'];
								 	$update_array	=	array(
										'level_type'  		=> '1',
										'nd_id'   		    => $row['nd_id'],
										'nd_code'   		=> $row['nd_code'],
										'nd_state_id'   	=> $state,
										'nd_city_id'   		=> $city,
										'nd_date'   		=> $date,
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$where_array	=	 array('imei'=> $row['imei']);
									$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
									$update_array1	=	array(
										'item_status'  		=> '1',
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$this->base_models->update_records('tbl_items',$update_array1,$where_array);	
								 }
							}	
					}

					if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    $this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					    // $this->session->set_flashdata('success','Query run successfully');
					}	

				

						
					if(!empty($data))
					{    	
						$page_data['acceptedprodct']	=	$data;
					}

					if(!empty($duplicate))
					{    	
	    				$page_data['rejectedprodct']	=	$duplicate;
					}

					if(!empty($sold))
					{    	
					    $page_data['soldprodct']		=	$sold;
					}
				 	$select	 = array('nd_id','nd_code','fname','lname');
					$where = array('status'=> '1');
					$page_data['data'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);
				 	$this->renderView('Admin/Sale/sale_items',$page_data);
				}
				
		}

    
//generate to excel	
public function generate_sale_excel($param1){
		// create file name
		$fileName = 'ItemSales'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Upload Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ND Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ND Firm Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Item Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'IMEI');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Status');
		
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, date('d-M-y', strtotime($element['upload_date'])));
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['nd_code']);
			$nd_code = $element['nd_code'];
			$sql = $this->db->query("select firmname from ndistributor where nd_code = '$nd_code'");
			$nd_array = $sql->result_array();
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $nd_array[0]['firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount,  $element['item_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount,  $element['imei']);
			$pay_status = ($element['item_status'] == '0') ? 'Unsold' : 'Sold';
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $pay_status);
			
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}			

	public function sale_list()
	{	
		
		$select	 = array('nd_id','nd_code','fname','lname');
		$where = array();
		$pagedata['nd_list'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);

        $select	 = array('stnd_id','item_code','imei','nd_code','item_status','upload_date');
		$where = array();
		$pagedata['delete_link'] = 'Adminsales/delete_item';
		$status ='';
		$imei ='';
		$nd_id ='';
		if($this->session->userdata('imei') || $this->session->userdata('status')|| $this->session->userdata('nd_id')){
			$this->session->userdata('imei');
			$this->session->userdata('status');
			$this->session->userdata('nd_id');
		}
    
      //Pagination Start
		$config = array();
        $config["base_url"] = site_url() . "/Adminsales/sale_list";
        $config["total_rows"] = $this->base_models->get_count('stnd_id','tbl_sales_to_nd', $where);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $pagedata["links"] = $this->pagination->create_links();
        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_nd', $where,'stnd_id',$config["per_page"], $page);     
       	//Pagination End

       	$imei = (@$imei) ? $imei : '';
       	$sstatus = (@$status) ? $status : null;
       	@strcmp($status,$sstatus);
       	$nd_id1 = (@$nd_id) ? $nd_id : null;
       	@strcmp($nd_id,$nd_id1);
        $pagedata['select']=array('status'=>$status,'imei'=>$imei,'nd_id'=>$nd_id);  
        $this->renderView('Admin/Sale/sale_list',$pagedata);
	}


	// with ci pagination in php
	public function sale_list_sess()
	{

		$select	 = array('nd_id','nd_code','fname','lname');
		$where = array('status'=> '1');
		$pagedata['nd_list'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);
       // print_r($_POST);
	    $select	 = array('stnd_id','item_code','imei','nd_code','item_status','upload_date');
		$where = array();
		$pagedata['delete_link'] = 'Adminsales/delete_item';
		$status ='';
		$imei ='';
		//Filter Process
	
       if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls')
       {
       	  	$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
       		$imei = trim($imei_no);

       	    $status = (@$this->input->post('status')!= null) ? $this->input->post('status') : '';
          
           $nd_id = (@$this->input->post('nd_id')!= null) ? $this->input->post('nd_id') : '';
           $array_items = $this->session->set_userdata(array("imei"=>$imei,"status"=>$status,"nd_id"=>$nd_id));
            if($imei !=''){
            	$filter =  array('imei'=> $imei);
            	$where = array_merge($where,$filter);	
            }  
            if($status !=''){
           		$filter =  array('item_status'=>$status);
            	$where = array_merge($where,$filter);	
            } 
             if($nd_id !=''){
           		$filter =  array('nd_id'=>$nd_id);
            	$where = array_merge($where,$filter);	
            }else{
            } 
		}else{
				if($this->session->userdata('imei') != NULL){
				$imei = $this->session->userdata('imei'); 
				$filter =  array('imei'=> $imei);
				$where = array_merge($where,$filter);
				} 
				if($this->session->userdata('status') != NULL){
				$status = $this->session->userdata('status'); 
				$filter =  array('item_status'=>$status);
				$where = array_merge($where,$filter);
				}
				if($this->session->userdata('nd_id') != NULL){
				$nd_id = $this->session->userdata('nd_id'); 
				$filter =  array('nd_id'=>$nd_id);
				$where = array_merge($where,$filter);
				}
		 }
       		/*echo '<pre>';
		print_r($where); die();*/
       	 if(@$_POST['submit']=='createxls')
       	  {

       	  //	$select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei');
			//$where .= array('status'=> '1');
			$data['data'] = $this->item_sales->GetAllItemValues('tbl_sales_to_nd', $where, $select);
	     /* echo '<pre>';
	       print_r($data['data']);*/
			//Export xls
				$this->generate_sale_excel($data['data']);			
				

          }
		//End Filter Process
    
      //Pagination Start
		$config = array();
        $config["base_url"] = site_url() . "/Adminsales/sale_list_sess";
        $config["total_rows"] = $this->base_models->get_count('stnd_id','tbl_sales_to_nd', $where);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $pagedata["links"] = $this->pagination->create_links();
        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_nd', $where,'stnd_id',$config["per_page"], $page);     
       	//Pagination End

       	$imei = (@$imei) ? $imei : '';

       	$sstatus = (@$status) ? $status : null;
        @strcmp($status,$sstatus);
        $nd_id1 = (@$nd_id) ? $nd_id : null;
       	@strcmp($nd_id,$nd_id1);
       @$pagedata['select']=array('status'=>$status,'imei'=>$imei,'nd_id'=>$nd_id);  
       $this->renderView('Admin/Sale/sale_list',$pagedata);
	}	



	public function edit_sale_item()
	{
		$select	 = array('nd_id','nd_code','fname','lname');
		$where = array('status'=> '1');
		$pagedata['nd_list'] = $this->item_sales->GetAllItemValues('ndistributor', $where, $select);

		$id = base64_decode($_GET['id']); 
		$select	 = array('stnd_id','nd_id','upload_date','imei');
		$where = array('stnd_id'=> $id);
		$pagedata['data']=$this->item_sales->GetAllItemValues('tbl_sales_to_nd', $where, $select);
		$this->renderView('Admin/Sale/edit_sale_items',$pagedata);
	}


	public function update_sale_items()
	{
		if (isset($_POST)) 
		{

			$imei = trim($this->input->post('imei'));
			$nd_id = $this->input->post('nd_id');
			$datef 	= date("Y-m-d", strtotime($_POST['upload_date'])); 
			$time = date("h:i:s");
	   		$date= $datef.' '.$time;

			$item_id = $this->input->post('stnd_id');
			
			$select	 = array('nd_code');
			$where = array('nd_id'=> $nd_id);
			$arr_nd	=$this->item_sales->GetAllItemValues('ndistributor', $where, $select);
            $nd_code =$arr_nd[0]['nd_code'];    
			$update_array	=	array(
										'nd_id'  		=> $this->input->post('nd_id'),
										'nd_code'  		=> $nd_code,
										'upload_date'  		=> $date,
										'updated_on'	=> date("Y-m-d H:i:s")
									);
			$where_array	=	 array('imei'=>$imei);
            
            $this->base_models->update_records('tbl_sales_to_nd',$update_array,$where_array);
			
			$nd_id1	=	$this->input->post('nd_id');
			$nd 	=	$this->db->query("select state,city from ndistributor where nd_id='$nd_id'");
			$nd_array = $nd->result_array();
			$state  = 	$nd_array[0]['state'];
			$city  	= 	$nd_array[0]['city'];
			$update_array1	=	array(
										'nd_id'  		=> $this->input->post('nd_id'),
										'nd_code'  		=> $nd_code,
										'nd_state_id'   	=> $state,
										'nd_city_id'   		=> $city,
										'nd_date'  		=> $date,
										'updated_on'	=> date("Y-m-d H:i:s")
									);
			$where_array1	=	 array('imei'=>$imei);


			if($this->base_models->update_records('tbl_item_sales',$update_array1,$where_array1) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
		}
			redirect(base_url('Adminsales/edit_sale_item/?id='.base64_encode($item_id)));	
	}	
			public function delete_sale_item()
			{
				 $imei = trim($_GET['id']); 
				$current_date = date("Y-m-d H:i:s");

				$update_array1 = array(
									'level_type'=>'0',
									'nd_id'=>'0',
									'nd_code'=>null,
									'nd_date'=>null,
									'nd_state_id' => null,
									'nd_city_id'  => null,
									'updated_on'=>$current_date
									);
				$where_array1 = array('imei'=>$imei);
				$this->base_models->update_records('tbl_item_sales',$update_array1,$where_array1);

				$update_array = array(
									'item_status'=>'0',
									'updated_on'=>$current_date
									);
				$where_array = array('imei'=>$imei);
				if($this->base_models->update_records('tbl_items',$update_array,$where_array) == true){
					$this->base_models->delete_records('tbl_sales_to_nd',array('imei'=>$imei));//delete item from tbl item sale to nd
					   $this->session->set_flashdata('success','Item Deleted Successfully From Sale List');
				}else{
				  $this->session->set_flashdata('error','Item Not Deleted Successfully From Sale List');
				}
			redirect('Adminsales/sale_list/');
			}
   
       
    public function check_imei_exist()
	{
		$rs 			= $this->db->query("select imei from tbl_items where status='1'");
		$array 			= $rs->result_array();
		$imei_array 	= array_column($array, 'imei');	 // for converting in 1 array format
		$imei_array1 	= array_flip($imei_array);

		$imei 	        = $_POST['imei'];

		$imei_code 		= trim($imei); // remove spaces from both sides of string

			 if(array_key_exists($imei_code, $imei_array1))
			 {
				echo 'error';
			 }
			 else
			 {
			 	echo 'success';
			 }	
	}	 

	//---------- ND ------------//
	public function national_distributor()
	{		
		$select = array('nd_id','nd_code','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select date_time from recent_login_user where user_code = nd_code AND type = "1" Order by id desc limit 1) as last_login');
		$pagedata['results'] = $this->base_models->GetAllValues('ndistributor',null, $select);
		echo '<pre>';
		print_r($pagedata['results']);
		die();
		$pagedata['delete_link'] = 'Adminmaster/delete_nd';
		$this->renderView('Admin/Master/national_distributor',$pagedata);
	}
	
	
	
	public function insert_nd()
	{
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            if(!empty($_FILES['image']['name'])){
                $config['upload_path'] = 'uploads/admin/users/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
					$data = $this->upload->data();
				}else{
                    $imageerrors = $this->upload->display_errors();
					$this->form_validation->set_message('image', $imageerrors);					
                }
			}
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>md5($this->input->post('inputPassword')),
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						'profile_pic'=>$data['file_name'],
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('wwc_admin',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(base_url('admin/users'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('Admin/add-user');
	}
	
	public function edit_nd()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->get_users('',$id);
		$this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function update_nd()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('admin/users')); 
		}
		
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>$password,
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(base_url('admin/edit_user/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_nd()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- END ND ------------//
	
		
}
