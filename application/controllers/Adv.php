<?php
/* READ ME FIRST
	<!-- *Different Types*-->
	User_type : 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
	Advertsiment types : 0=pending, 1=rejected, 2=approved, 3=publish
	
	Note: Load this script in every page of admin
	<!-- Sparkline chart JavaScript -->
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
	<!-- toast CSS -->
	<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
	
	<!-- Load Common/Admin Custome JS ON each page-->
	<script src="<?php echo base_url();?>assets/js/common/common.js"></script>
	<script src="<?php echo base_url();?>assets/js/admin/admin.js"></script>
	
*/
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Adv extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 3 && $this->session->userdata('user_type') != 4 && $this->session->userdata('user_type') != 5 ){
			$this->session->set_flashdata('error', 'You Are not Allowed to access...!');
			redirect(site_url('login'));
		}

	}
	
	public function index(){
		$this->manage_adv();
	}
	
	public function invoice(){
		$id = base64_decode($_GET['id']);		
		if($id==''){
			$this->session->set_flashdata('error','Invoice not found');
			redirect(site_url('adv/manage_adv/3/invoice/1')); 
		}
		$this->load->helper('common_helper');
		$invoice_details = $this->base_models->get_invoice_details($id);
		$pagedata = array('data'=>$invoice_details,'title'=>'Invoice');
		$this->renderView('Adv/invoice',$pagedata);
	}
	
	//generate to excel	
	public function generate_adv_excel($param1,$param2){
		// create file name
		$fileName = $param1.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param2;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Business Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Edition');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Ro.No.');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Ro.Code.');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Schedule Type');       
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Time');       
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Date');       
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Bank Name');       
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Pay Method');       
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Pay/Chq.No');       
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Amt');       
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Final(GST+Amt)');       
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'PayStatus');       
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['username']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['business_name']);
				$citysName = array();
				$x= 0;
				$cities_id = explode(',',$element['city_id']);
				foreach($cities_id as $city_id){
					$citysName[] =  getCityName($cities_id[$x]); 
					$x++;
				}				
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, implode(',', $citysName));
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['ro_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['id']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['schedule_type']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['time']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, date('d-M-y', strtotime($element['date'])));
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['bank_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['pay_method']);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['pay_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['amt']);
			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['net_amt']);
				$pay_status = ($element['pay_status'] == '0') ? 'Pending' : 'Received';
			$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $pay_status);
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}	
	
	public function manage_adv($status,$param1 = '',$param2 = '',$param3 = '')
	{
		$this->load->helper('common_helper');
		switch($status){
			case '0':
				$status_str = 'Advertsiment Pending';
				break;
			case '1':
				$status_str = 'Advertsiment Reject';
				break;
			case '2':
				$status_str = 'Advertsiment Approved';
				break;
			case '3':
				$status_str = 'Advertsiment Publish';
				break;
		}
		
		//Start Where conditions
			$where = '';
			if($param1 == 'date' || $param3 == 'date'){						
				if(!$this->input->post('daterange')){
					redirect(site_url().'adv/manage_adv/'.$status);
				}
				$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0]));
				$todate = date('Y-m-d', strtotime($ranges[1]));
				$client_id = (@$this->input->post('client_id')) ? $this->input->post('client_id') : '';
				$user_id = (@$this->input->post('user_id')) ? $this->input->post('user_id') : '';
		
				$where = "AND ta.date BETWEEN '$fdate' AND '$todate'";
				if($this->input->post('client_id') != ''){
					$where .= "AND ta.client_id = $client_id";
				}
				if($this->input->post('user_id') != ''){
					$where .= " AND ta.user_id = $user_id";
				}
			}
			
			if($param1 == 'invoice'){		
				switch($param2){
					case '1':
						$status_str = 'Invoice Approved';
						break;
					case '2':
						$status_str = 'Invoice Pending';
						break;
				}
				$where .= ' AND ta.invoice_generate = '.$param2;
			}else{			
				switch($this->session->userdata('login_type')){
					case 'Marketing' :
						$where .= ' AND ta.user_id = '.$this->session->userdata('id');
						break;
				}
			}
			if($this->session->userdata('login_type')=='Marketing'){//For marketing
	        	$where .= ' AND ta.user_id = '.$this->session->userdata('id');	        
			}
		//End Where conditions
		
		$data['data'] = $this->base_models->get_adv_data($status,$where);
		
		//Export xls
		if($param1 == 'createxls' || $param3 == 'createxls' || @$this->input->post('submit') == 'createxls'){
			$this->generate_adv_excel($status_str,$data['data']);			
		}
		
		$clients_details = $this->base_models->get_records('tbl_client',array('id','business_name'),'','');
		$citys_details = $this->base_models->get_records('tbl_city',array('id','name'),'','');
		$users = $this->base_models->get_records('wwc_admin',array('id','username'),array('type' => 3),'');
		$client_id = (@$client_id) ? $client_id : '';
		$user_id = (@$user_id) ? $user_id : '';
		$id=$this->session->userdata('id');
		$pagedata = array('select'=>array('client_id' =>$client_id,'user_id' =>$user_id),'clients_details'=>$clients_details,'users_details'=>$users,'citys_details'=>$citys_details,'data'=>$data['data'],'status'=>$status, 'delete_link'=>'Adv/delete_adv', 'title' => $status_str, 'invoice' => $param2);

		$this->renderView('Adv/manage_adv',$pagedata);
	}	
	
	public function change_status()
	{
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
						$_POST['col']=>$_POST['status'],
						'updated_by'=>$current_date,
						'lastUpdateUser'=>$this->session->userdata('id')
						);
		$where_array = array('id'=>$_POST['id']);
		
		if($this->base_models->update_records('tbl_adv',$update_array,$where_array)){
			echo "success";
			// $this->session->set_flashdata('success','Status change successfully');
		}else{
			// $this->session->set_flashdata('error','Error while changing status');
			echo "failed";
		}
	}

	public function add_adv()
	{
		if($_POST != ''){
			$this->form_validation->set_rules('client_id', 'Client', 'trim|required');
			// $this->form_validation->set_rules('city_id', 'City', 'trim|required');
			// $this->form_validation->set_rules('ro_no', 'Ro.No.', 'trim|required');
			$this->form_validation->set_rules('schedule_type', 'Schedule Type', 'trim|required');
			$this->form_validation->set_rules('time', 'Time', 'trim|required');
			$this->form_validation->set_rules('date', 'date', 'date|required');
			//$this->form_validation->set_rules('content', 'Content', 'trim|max_length[10000]');
			$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim');
			$this->form_validation->set_rules('pay_method', 'Payment Method', 'trim|required');
			$this->form_validation->set_rules('pay_no', 'Payment/Cheque No.', 'trim');
			$this->form_validation->set_rules('pay_status', 'Payment Status', 'trim|required');
			$this->form_validation->set_rules('amt', 'Amount', 'trim|required|numeric');
			$this->form_validation->set_rules('net_amt', 'Final Amount', 'trim|required|numeric');
			$this->form_validation->set_rules('rate', 'Rate', 'trim|required|numeric');
			$this->form_validation->set_rules('remark', 'Remark', 'trim|required');
			
			$current_date = date("Y-m-d H:i:s");
			$error='';			
				// if (empty($_FILES['image']['name'][0])){
					// $this->form_validation->set_rules('image', 'Image', 'required');
				// }
				if(!empty($_FILES['image']['name'])){
					$config['upload_path'] = 'uploads/admin/adv/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$this->upload->initialize($config);
					if ($this->upload->do_upload('image')) {
						$data = $this->upload->data();
					}else{
						$imageerrors = $this->upload->display_errors();
						$this->form_validation->set_message('image', $imageerrors);	
					}
				}else{
					$data['file_name'] = '';
				}
				
				if(!empty($_FILES['ro_image']['name'])){
					$config['upload_path'] = 'uploads/admin/adv/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$this->upload->initialize($config);
					if ($this->upload->do_upload('ro_image')) {
						$data1 = $this->upload->data();
					}else{
						$imageerrors = $this->upload->display_errors();
						$this->form_validation->set_message('ro_image', $imageerrors);	
					}
				}else{
					$data1['file_name'] = '';
				}
			
				// else{
					// $imageerrors="Select an image";
					// $this->form_validation->set_message('error', $imageerrors);
				// }
				
				// if (empty($_FILES['ro_image']['name'][0])){
					// $this->form_validation->set_rules('ro_image', 'RO Image', 'required');
				// }
				// if(!empty($_FILES['ro_image']['name'])){
					// $config['upload_path'] = 'uploads/admin/adv/';
					// $config['allowed_types'] = 'gif|jpg|png';
					// $this->upload->initialize($config);
				// }else{
					// $imageerrors="Select an RO Image";
					// $this->form_validation->set_message('error', $imageerrors);
				// }
				
				if($this->form_validation->run())
				{
					//Check if already exist booking
					$citys= implode(",", array_values($this->input->post('city_id')));
					$chk_array=array('date'=>$this->input->post('date'),'time'=>$this->input->post('time'));
					$this->db->where($chk_array);
					$this->db->where_in('city_id', $this->input->post('city_id'));
					$result = $this->db->get('tbl_booking');	
					if($result->num_rows()>0){
						$this->session->set_flashdata('error','Sorry You where late, This Edition already booked recently');
						redirect(site_url('adv/add_adv'));
					}
					//End Check
					
					// if ($this->upload->do_upload('image'))
					// {
						// $data = $this->upload->data();
					// if ($this->upload->do_upload('ro_image'))
					// {
						// $data1 = $this->upload->data();
						$insert_array=array(
								'user_id'=>$this->session->userdata('id'),
								'client_id'=>$this->input->post('client_id'),
								'city_id'=> $citys,
								'ro_no'=>$this->input->post('ro_no'),
								'schedule_type'=>$this->input->post('schedule_type'),
								'time'=>$this->input->post('time'),
								'date'=> $this->input->post('date'),
								'content'=>$this->input->post('content'),
								'rate'=>$this->input->post('rate'),
								'bank_name'=>$this->input->post('bank_name'),
								'pay_method'=>$this->input->post('pay_method'),
								'pay_no'=>$this->input->post('pay_no'),
								'pay_status'=>$this->input->post('pay_status'),
								'amt'=>$this->input->post('amt'),
								'net_amt'=>$this->input->post('net_amt'),
								'remark'=>$this->input->post('remark'),
								'image'=>$data['file_name'],
								'ro_image'=>$data1['file_name'],
								'created_by'=>date("Y-m-d H:i:s"),
								'status'=> '0',
								'lastUpdateUser'=>$this->session->userdata('id')
							);
						$this->db->trans_begin();// transaaction begains
						if($this->base_models->add_records('tbl_adv',$insert_array)){
							$insert_id = $this->db->insert_id();
							$insert_array += [ 'adv_id'=> $insert_id ];
							// array_push($insert_array,array('adv_id'=> $insert_id));
							// $insert_array .= array(
											// 'adv_id'=> $insert_id
											// );
							$this->base_models->add_records('adv_changes',$insert_array);
							
							$arrs = array_values($this->input->post('city_id'));
							// print_r($arrs);
							$x=0;
							foreach ($arrs as $arr ){
								$insert_booking=array(
									'date'=> $this->input->post('date'),
									'city_id'=>$arrs[$x],
									'adv_id'=>$insert_id,
									'city_rate'=>$this->input->post('city_rate')[$x],
									'schedule_type'=>$this->input->post('schedule_type'),
									'time'=>$this->input->post('time'),
									'created_by'=>date("Y-m-d H:i:s")
								);
								$this->base_models->add_records('tbl_booking',$insert_booking);
								$x++;
							}
							if($this->db->trans_status() === FALSE){
								$this->db->trans_rollback();
								$this->session->set_flashdata('error','Database error try again');
							}else{
								$this->db->trans_commit();
								$UserName = $this->base_models->GetSingleDetails('wwc_admin',array('id'=>$this->session->userdata('id')), array('username'))->username;
								$ClientName = $this->base_models->GetSingleDetails('tbl_client',array('id'=>$this->input->post('client_id')), array('business_name'))->business_name;
								$pubDate = date('d-M-Y', strtotime($this->input->post('date')));
								$time = $this->input->post('time');
								$msg = "$UserName, $ClientName, PublishDate: $pubDate, Slot: $time, Ro.No: $insert_id";
								$this->message_send($msg, '9881098078');
								$this->session->set_flashdata('success','Added successfully');
							}
						}else{
							$this->session->set_flashdata('error','Not added successfully');
						}
								redirect(site_url('adv/add_adv'));
					// }else{
						// $imageerrors = $this->upload->display_errors();
						// $this->form_validation->set_message('RO image', $imageerrors);					
					// }
				}
			}
			// $admin_details = $this->base_models->get_records('wwc_admin','',array('id'=>$this->session->userdata('id')),'');
			$clients_details = $this->base_models->get_records('tbl_client',array('id','business_name'),array('status' =>'1'),'');
			$citys_details = $this->base_models->get_records('tbl_city',array('id','name'),'','');
		
			$pagedata = array('clients_details'=>$clients_details,'citys_details'=>$citys_details,'error'=>$error);
			$this->renderView('Adv/add-adv',$pagedata);
	}
	
	public function valid_url_format($str){

        $pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
        if (!preg_match($pattern, $str)){
            $this->set_message('valid_url_format', 'The URL you entered is not correctly formatted.');
            return FALSE;
        }
        return TRUE;
    }       
	
	public function delete_adv(){
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'4',
							'deleted_by'=>$current_date,
							'lastUpdateUser'=>$this->session->userdata('id')
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('tbl_adv',$update_array,$where_array) == true){
			
			$this->base_models->delete_records('tbl_booking',array('adv_id'=>$id));//delete booking
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	
		
	public function updateAdv()
	{
		$current_date = date("Y-m-d H:i:s");
		$bank_name = $this->input->post('bank_name');
		$pay_no = $this->input->post('pay_no');
		$invoice_date = $this->input->post('invoice_date');
		$invoice_generate = $this->input->post('invoice_generate');
		$invoice_no = $this->input->post('invoice_no');
		$pay_method = $this->input->post('pay_method');
		$supplier_ref = $this->input->post('supplier_ref');
		
			$update_array = array(
							'bank_name'=>$bank_name,
							'pay_no'=>$pay_no,
							'invoice_date'=>$invoice_date,
							'invoice_generate'=>$invoice_generate,
							'invoice_no'=>$invoice_no,
							'pay_method'=>$pay_method,
							'supplier_ref'=>$supplier_ref
							);
			$where_array = array('id'=>$_POST['id']);
			
			if($this->base_models->update_records('tbl_adv',$update_array,$where_array)){
				$data['status'] = 'success';
				$data['message'] = 'Status change successfully';
				// echo "success";
				// $this->session->set_flashdata('success','Status change successfully');
			}else{
			    // $this->session->set_flashdata('error','Error while changing status');
				$data['status'] = 'fail';
				$data['message'] = 'Error while changing status';
				// echo "failed";
			}
			echo json_encode($data);
			
	}
	
	public function edit_adv()
	{
		$id = base64_decode($_GET['id']);
		$status = base64_decode($_GET['status']);
		
		if($id==''){
			redirect(site_url('adv/manage_adv/'.$status)); 
		}

		// if(isset($_POST) != ''){
		
			$this->form_validation->set_rules('client_id', 'Client', 'trim|required');
			// $this->form_validation->set_rules('city_id', 'City', 'trim|required');
			// $this->form_validation->set_rules('ro_no', 'Ro.No.', 'trim|required');
			$this->form_validation->set_rules('schedule_type', 'Schedule Type', 'trim|required');
			$this->form_validation->set_rules('time', 'Time', 'trim|required');
			$this->form_validation->set_rules('date', 'date', 'date|required');
			//$this->form_validation->set_rules('content', 'Content', 'trim|max_length[10000]');
			$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim');
			$this->form_validation->set_rules('pay_method', 'Payment Method', 'trim|required');
			$this->form_validation->set_rules('pay_no', 'Payment/Cheque No.', 'trim');
			//$this->form_validation->set_rules('status', 'Status', 'trim|required');
			$this->form_validation->set_rules('pay_status', 'Payment Status', 'trim|required');
			$this->form_validation->set_rules('amt', 'Amount', 'trim|required|numeric');
			$this->form_validation->set_rules('net_amt', 'Net Amount', 'trim|required|numeric');
			$this->form_validation->set_rules('rate', 'Rate', 'trim|required|numeric');
			$this->form_validation->set_rules('remark', 'Remark', 'trim|required');
			
			$current_date = date("Y-m-d H:i:s");
			$error='';
			
			if($this->form_validation->run()){				
				if(!empty($_FILES['image']['name']))
				{
					$config1['upload_path'] = 'uploads/admin/adv/';
					$config1['allowed_types'] = 'gif|jpg|png|jpeg';
					$this->upload->initialize($config1);
					if ($this->upload->do_upload('image'))
					{
						$data = $this->upload->data();
						//print_r($data);exit;
						$image=$data['file_name'];
						unlink("uploads/admin/adv/".$this->input->post('image1'));
					}
				}else{
					$image=$this->input->post('image1');
				}
				
				if(!empty($_FILES['ro_image']['name']))
				{
					$config1['upload_path'] = 'uploads/admin/adv/';
					$config1['allowed_types'] = 'gif|jpg|png|jpeg';
					$this->upload->initialize($config1);
					if ($this->upload->do_upload('ro_image'))
					{
					// $error = array('error' => $this->upload->display_errors());				
						$data1 = $this->upload->data();
						//print_r($data);exit;
						$ro_image1=$data1['file_name'];
						unlink("uploads/admin/adv/".$this->input->post('ro_image1'));
					}
				}else{
					$ro_image1=$this->input->post('ro_image1');
				}
				$citys= implode(",", array_values($this->input->post('city_id')));
				$update_array = array(
									'client_id'=>$this->input->post('client_id'),
									'city_id'=>$citys,
									'ro_no'=>$this->input->post('ro_no'),
									'schedule_type'=>$this->input->post('schedule_type'),
									'time'=>$this->input->post('time'),
									'date'=> $this->input->post('date'),
									'content'=>$this->input->post('content'),
									'rate'=>$this->input->post('rate'),
									'bank_name'=>$this->input->post('bank_name'),
									'pay_method'=>$this->input->post('pay_method'),
									'pay_no'=>$this->input->post('pay_no'),
									'pay_status'=>$this->input->post('pay_status'),
									'amt'=>$this->input->post('amt'),
									'net_amt'=>$this->input->post('net_amt'),
									'remark'=>$this->input->post('remark'),
									'image'=>$image,
									'ro_image'=>$ro_image1,
									'lastUpdateUser'=>$this->session->userdata('id'),
									'updated_by'=>$current_date
									//'status'=> $this->input->post('status')
									);
				
				$where_array = array('id'=>$id);
				if ($this->base_models->update_records('tbl_adv',$update_array,$where_array)){
					$update_array += [ 'user_id'=>$this->session->userdata('id'), 'adv_id'=> $id, 'created_by'=>$current_date ];
					$this->base_models->add_records('adv_changes',$update_array);
					
					$this->base_models->delete_records('tbl_booking',array('adv_id'=>$id));//delete booking
					$arrs = array_values($this->input->post('city_id'));
					// print_r($arrs);
					$x=0;
					foreach ($arrs as $arr ){
						$insert_booking=array(
							'date'=> $this->input->post('date'),
							'city_id'=>$arrs[$x],
							'adv_id'=>$id,
							'city_rate'=>$this->input->post('city_rate')[$x],
							'schedule_type'=>$this->input->post('schedule_type'),
							'time'=>$this->input->post('time'),
							'created_by'=>date("Y-m-d H:i:s")
						);
						$this->base_models->add_records('tbl_booking',$insert_booking);// add new booking again
					$x++;
					}
							
					$this->session->set_flashdata('success','Updated successfully');
					redirect(site_url('adv/manage_adv/'.$status)); 
				}else {
					$this->session->set_flashdata('error','Error while updating');
					redirect(site_url('adv/manage_adv/'.$status)); 
				}
			}
		// }
		$this->load->helper('common_helper');
		
		// $admin_details = $this->base_models->get_records('wwc_admin','',array('id'=>$this->session->userdata('id')),'');
		$clients_details = $this->base_models->get_records('tbl_client',array('id','business_name'),'','');
		$citys_details = $this->base_models->get_records('tbl_city',array('id','name'),'','');
		
		$pagedata = array('clients_details'=>$clients_details,'citys_details'=>$citys_details,'status'=>$status,
							'data'=>$this->base_models->get_records('tbl_adv','',array('id'=>$id,'status !='=>'4'),''),
							'error'=>$error);
		$this->renderView('Adv/edit-adv',$pagedata);
	}

		
	//entry already exist or not 
	public function check_booking(){
		$date = $this->input->post('date');
		$city_id = $this->input->post('city_id');
		$schedule_type = $this->input->post('schedule_type');
		$time = $this->input->post('time');
		
		$data['status'] = 'nothing';
		$data['message'] = '';
		if($city_id != ''){
			// $citys= implode(",", $this->input->post('city_id'));
			$chk_array=array('date'=>$date,'time'=>$time);
			// $chk_array=array('date'=>$date,'schedule_type'=>$schedule_type,'time'=>$time);

			$this->db->where($chk_array);
			$this->db->where_in('city_id', $city_id);
			$result = $this->db->get('tbl_booking');
			if($result->num_rows()>0){  
				$data['status'] = 'fail';
				$data['message'] = 'Booking on this date is not available';
			}else{
				$data['status'] = 'success';
				$data['message'] = 'Booking is available';
			}
		}
				echo json_encode($data);
	}
	
	public function viewContent(){
		$id  = $this->input->post('id');
		$result = $this->base_models->get_records('tbl_adv',array('content'),array('id'=>$id),true);
		$data['status'] = 'success';
		$data['message'] = $result['content'];
		echo json_encode($data);
	}
		
}
