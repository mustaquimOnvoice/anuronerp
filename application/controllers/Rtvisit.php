<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Rtvisit extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function rt_visit_list()
	{
		$select	 = array('rv.rt_visit_id', 'rv.visit_date', 'rv.visit_time', 'rv.lati', 'rv.longi', 'rv.atsm_id', 'rv.atsm_code','rv.rt_code', '(select firmname from retailer where rt_id = rv.rt_id ORDER BY rt_id LIMIT 1) as rt_firmname', '(select fname from atsm where atsm_id = rv.atsm_id ORDER BY atsm_id LIMIT 1) as tsm_fname', '(select mname from atsm where atsm_id = rv.atsm_id ORDER BY atsm_id LIMIT 1) as tsm_mname', '(select lname from atsm where atsm_id = rv.atsm_id ORDER BY atsm_id LIMIT 1) as tsm_lname', '(select image_url from images where ref_code = rv.rt_visit_id AND type="3" ORDER BY id) as image');
		$where = array();
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Rtvisit/rt_visit_list";
		$config["total_rows"] = $this->base_models->get_count('rt_visit_id','rt_visit', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination_data($select,'rt_visit as rv',$where = NULL,$orderby= 'rv.rt_visit_id',$config["per_page"], $page);     
		//Pagination End		
		$this->renderView('Admin/Report/rt_visit_list',$pagedata);
	}

	// with ci pagination in php
	public function rt_visit_list_sess()
	{
		$select	 = array('rv.rt_visit_id', 'rv.visit_date', 'rv.visit_time', 'rv.lati', 'rv.longi', 'rv.atsm_id', 'rv.atsm_code','rv.rt_code', '(select firmname from retailer where rt_id = rv.rt_id ORDER BY rt_id LIMIT 1) as rt_firmname', '(select fname from atsm where atsm_id = rv.atsm_id ORDER BY atsm_id LIMIT 1) as tsm_fname', '(select mname from atsm where atsm_id = rv.atsm_id ORDER BY atsm_id LIMIT 1) as tsm_mname', '(select lname from atsm where atsm_id = rv.atsm_id ORDER BY atsm_id LIMIT 1) as tsm_lname', '(select image_url from images where ref_code = rv.rt_visit_id AND type="3" ORDER BY id) as image');
		$where = array();

		//Filter Process	
	   	if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls'){
			$fdate = (@$this->input->post('fromdate')) ? $this->input->post('fromdate').' 00:00:00' : '';
			$todate = (@$this->input->post('todate')) ? $this->input->post('todate').' 23:59:00' : '';			
			   
			if($this->input->post('fromdate') != '' && $this->input->post('todate') != ''){
				$filter =  array('created_at >='=> "$fdate", 'created_at <='=> "$todate", );
				$where = @array_merge($where,$filter);
			}
		}
		
		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->base_models->get_pagination_data($select,'rt_visit as rv',$where,$orderby= 'rv.rt_visit_id',NULL,NULL);     
			//Export xls
			$this->generate_rt_visit_list_excel($data['data']);	
		}
		
		//End Filter Process
	   	//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Rtvisit/rt_visit_list_sess";
		$config["total_rows"] = $this->base_models->get_count('rt_visit_id','rt_visit', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination_data($select,'rt_visit as rv',$where,$orderby= 'rv.rt_visit_id',$config["per_page"], $page);     
		//Pagination End
		
		$this->renderView('Admin/Report/rt_visit_list',$pagedata);
	}

	//generate to excel	
	public function generate_rt_visit_list_excel($param1){
		// create file name
		$fileName = 'RTVisitList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'TSM Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'TSM Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'RT Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'RT Firmname Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'VisitDateTime');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Location');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['atsm_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['tsm_fname'].' '.$element['tsm_mname'].' '.$element['tsm_lname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['rt_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['rt_firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, date('d-M-Y',strtotime($element['visit_date'])).' '.$element['visit_time']);
				$location = ($element['lati'] == null) ? 'No Location' : 'http://maps.google.com/maps?q='.$element['lati'].','.$element['longi'];
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $location);
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
		
}
