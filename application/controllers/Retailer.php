<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Retailer extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 5){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function faulty_return_list()
	{	
		$select	 = array('if_id','imei','reason','rt_date','d_date','admin_status','nd_status','d_status','(select image_url from images where type= 7 and ref_code = tbl_item_faulty.if_id limit 1) as image_url');
		$where = array('rt_id' => $this->session->userdata('id'));
		//$pagedata['delete_link'] = 'Adminitems/delete_item';
	
		
		//Pagination Start
		$config = array();
		$config["base_url"] = base_url() . "Retailer/faulty_return_list";
		$config["total_rows"] = $this->base_models->get_count('if_id','tbl_item_faulty', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination_data($select,'tbl_item_faulty',$where,$orderby = 'if_id',$config["per_page"], $page);     
		//Pagination End
		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Retailer/Sale/faulty_return_list',$pagedata);
	}

	
	
	public function faulty_return()
	{		
		$this->renderView('Retailer/Sale/faulty_return');
	}
	public function return_faulty_items()
	{	
		if(empty($_POST)){
			redirect('Retailer/faulty_return/');
		}

		$all_imei 	= $_POST['imei'];
		$reason 	= $_POST['reason'];
		$arr_imei 	= preg_split('/\r\n|[\r\n]/', $all_imei);
		
		$rt_id =$this->session->userdata('id');
		      // array For product is in the listof d  or not 
				$rs = $this->db->query("select imei from tbl_sales_to_rt where rt_id='$rt_id'");
				$array = $rs->result_array();
				$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
		        $imei_array1 =  array_flip($imei_array);
				
				// array for product sold 
		        $rsold = $this->db->query("select imei from tbl_sales_to_rt where item_status='1'");
				$arraysold = $rsold->result_array();
				$sold_imei_array = array_column($arraysold, 'imei');	 // for converting in 1 array format
		        $sold_imei_array1 =  array_flip($sold_imei_array);
		           
		        // array for product is in faulty list or not 
		        $faulty = $this->db->query("select imei from tbl_item_faulty");
				$arrayfaulty = $faulty->result_array();
				$faulty_imei_array = array_column($arrayfaulty, 'imei');	 // for converting in 1 array format
		        $faulty_imei_array1 =  array_flip($faulty_imei_array);

				if (!empty($arr_imei)) 
				{
					$i=0;
				  foreach($arr_imei as $row)
				   {
					    $imei 	    = 	$arr_imei[$i]; 
						$imei_code  =	trim($imei); // remove spaces from both sides of string
						
						   if(array_key_exists($imei_code, $imei_array1))
							{
							
									if(array_key_exists($imei_code, $faulty_imei_array1))
									{
										$fault[] = array
										    (
												'imei' => $imei_code
											);
											 $this->session->set_flashdata('error','Given IMEI Number Already in Faulty List!');	
											 
									}
									else{	
											$rt = $this->db->query("select rt_code,d_id from retailer where rt_id='$rt_id'");
											$rt_array = $rt->result_array();
											$rt_code  = $rt_array[0]['rt_code'];
											$d_id  	  = $rt_array[0]['d_id'];

											$d = $this->db->query("select d_code from distributor where d_id='$d_id'");
											$d_array = $d->result_array();
											$d_code  = $d_array[0]['d_code'];
											$data[]   = array
												(
													'level_type' 		=>'2',
													'imei'   			=> $imei_code,
													'reason'   			=> $reason,
													'd_id'   			=> $d_id,
													'd_code'   			=> $d_code,
													'rt_id'   			=> $rt_id,
													'rt_code'   		=> $rt_code,
													'rt_date'			=> date("Y-m-d H:i:s"),
													'inserted_on'		=> date("Y-m-d H:i:s")
												);
									}	
								
							}
							else
							{
								$notinlist[] = array
									(
										'imei'=> $imei_code
									);
								 $this->session->set_flashdata('error','Given IMEI Number Not In Your Sale List!');	
								
							}		  
						$i++;
					}
					
					$this->db->trans_begin(); //trans start
					if(!empty($data))
					{
						foreach ($data as $row)
						{	 
							if($row['imei']!=''){	
								// $this->db->insert("tbl_item_faulty",$row);
								// $ref_id = $this->db->last_query();
								// $this->uploadImageFile($file,$user_id,$type=1);
								$ref_code= $this->Base_Models->AddValues ( "tbl_item_faulty", $row );

									//imgvideo update
									if($_FILES["file"]["name"] != ''){
										// File upload path
										$fileName = str_shuffle('doafaulty').'-'.basename($_FILES["file"]["name"]);
										$filePath = "uploads/doa/".$fileName;
									
										// Check the file type
										$allowedTypeArr = array("image/png","image/gif","image/jpg","image/jpeg","video/mp4", "video/avi", "video/mpeg", "video/mpg", "video/mov", "video/wmv", "video/rm");
										if(in_array($_FILES['file']['type'], $allowedTypeArr)){
											// Upload file to local server
											if(move_uploaded_file($_FILES['file']['tmp_name'], $filePath)){											
												$TableValues ['ref_code'] = $ref_code;
												$TableValues ['type'] = '7';
												$TableValues ['image_url'] = base_url().$filePath;
												$this->Base_Models->AddValues ( "images", $TableValues );
											}
										}
									}
							}
						}	
						$this->session->set_flashdata('success','You Have Done Faulty Entry Successfully ');
					}

					if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    //$this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					      //$this->session->set_flashdata('success','You Have Done Faulty Entry Successfully ');
					}	

				$this->renderView('Retailer/Sale/faulty_return',$page_data);
				}
				
	}


	//-- Anuron ERP--//
	public function dashboard()
	{		
		// $this->renderView('Retailer/dashboard');
		redirect('Retailer/stock_list');
	}
	
	public function stock_list()
	{	
		$select	 = array('strt_id','item_id','item_code','imei','rt_id','rt_code','inserted_on');
		$where = array('item_status'=> '0','rt_id' => $this->session->userdata('id'));
		//$pagedata['delete_link'] = 'Adminitems/delete_item';
	
		$imei ='';	
		if($this->session->userdata('imei') ){
			$this->session->userdata('imei');
		}
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Retailer/stock_list";
		$config["total_rows"] = $this->base_models->get_count('strt_id','tbl_sales_to_rt', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_rt', $where,'strt_id',$config["per_page"], $page);     
		//Pagination End

		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Retailer/Stock/stock_list',$pagedata);
	}

	// with ci pagination in php
	public function stock_list_sess()
	{
	   // print_r($_POST);
		$select	 = array('strt_id','item_id','item_code','imei','rt_id','rt_code','inserted_on');
		$where = array('item_status'=> '0','rt_id' => $this->session->userdata('id'));

		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls'){
			$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
			$imei = trim($imei_no);

			$array_items = $this->session->set_userdata(array("imei"=>$imei));
			if($imei !=''){
				$filter =  array('imei'=> $imei);
				$where = array_merge($where,$filter);	
			}else{
			} 
			if($this->session->userdata('imei') != NULL){
				$imei = $this->session->userdata('imei'); 
				$filter =  array('imei'=> $imei);
				$where = array_merge($where,$filter);
			} 
		}

		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->base_models->GetAllValues('tbl_sales_to_rt', $where, $select);
			$this->generate_item_excel($data['data']);			
		}

		$config = array();
		$config["base_url"] = site_url() . "/Retailer/stock_list_sess";
		$config["total_rows"] = $this->base_models->get_count('strt_id','tbl_sales_to_rt', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_rt', $where,'strt_id',$config["per_page"], $page);     
		//Pagination End

		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Retailer/Stock/stock_list',$pagedata);		
	}	
	
	public function activated_list()
	{	
		$select	 = array('strt_id','item_id','item_code','imei','rt_id','rt_code','inserted_on','updated_on');
		$where = array('item_status'=> '1','rt_id' => $this->session->userdata('id'));
		//$pagedata['delete_link'] = 'Adminitems/delete_item';
	
		$imei ='';	
		if($this->session->userdata('imei') ){
			$this->session->userdata('imei');
		}
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Retailer/activated_list";
		$config["total_rows"] = $this->base_models->get_count('strt_id','tbl_sales_to_rt', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_rt', $where,'strt_id',$config["per_page"], $page);  
		//echo $this->db->last_query();die;   
		//Pagination End

		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei,'activated_list'=>'');  
		$this->renderView('Retailer/Stock/activated_list',$pagedata);
	}

	public function activated_list_sess()
	{
		$where2 = "item_status = '1' AND rt_id =". $this->session->userdata('id');

		$imei = '';
		$fdate = '';
		$todate = '';
		$filter = array();
		
		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls'){
			$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
			$imei = trim($imei_no);
			$fdate = (@$this->input->post('fdate')) ? date('Y-m-d',strtotime($this->input->post('fdate'))) : '';
			$todate = (@$this->input->post('todate')) ? date('Y-m-d',strtotime($this->input->post('todate'))) : '';
			$array_items = $this->session->set_userdata(array("imei"=>$imei,"fdate"=>$fdate,"todate"=>$todate));
			$time1 = '00:00:00';
			$time2 = '23:59:59';
			
			if($fdate !=''){
				$where2 .= " AND updated_on between '$fdate $time1' and '$todate $time2'";
			}
			if($imei !=''){
				$where2 .=  " AND imei = '$imei'";
			} 
		}

		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->base_models->Custome_quary("SELECT *
																FROM tbl_sales_to_rt
																where $where2
																ORDER BY strt_id DESC");
			$this->generate_item_excel($data['data']);			
		}

		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Retailer/activated_list_sess";
		$total_rows = $this->base_models->Custome_quary("SELECT count(strt_id) as counting FROM tbl_sales_to_rt where $where2");
		$config["total_rows"] = $total_rows[0]['counting'];
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$limit = $config["per_page"];
		$pagedata['results'] = $this->base_models->Custome_quary("SELECT *
																FROM tbl_sales_to_rt
																where $where2
																ORDER BY strt_id DESC
																 LIMIT $limit OFFSET $page");     
		//Pagination End

		$imei = (@$imei) ? $imei : '';
		$fdate = (@$fdate) ? $fdate : '';
		$todate = (@$todate) ? $todate : '';
		$pagedata['select']=array('imei'=>$imei,'fdate'=>$fdate,'todate'=>$todate);  
		$this->renderView('Retailer/Stock/activated_list',$pagedata);		
	}

	//generate to excel	
	public function generate_item_excel($param1){
		// create file name
		$fileName = 'ItemList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'IMEI');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Item Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Stock Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Activated Date');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['imei']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['item_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-M-y', strtotime($element['inserted_on'])));
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, date('d-M-y', strtotime($element['updated_on'])));
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	public function updateStock()
	{
		$current_date = date("Y-m-d H:i:s");
		$cilent_name = $this->input->post('cilent_name');
		$cilent_mob = $this->input->post('cilent_mob');
		$imei_code = trim($this->input->post('imei_code'));
		$rt_code =	 $this->session->userdata('code');
		$this->db->trans_begin();// transaction start
		
		//insert client
		$cldata  = array('rt_id' => $this->session->userdata('id'),
						'fname'   => $cilent_name,
						'contact'   => $cilent_mob,
						'inserted_on'=> date('Y-m-d h:i:s')
						);
		$this->db->insert("client",$cldata);
		$insert_id = $this->db->insert_id();
		//fetch client details
		$cl = $this->db->query("select c_id,c_code from client where c_id='$insert_id'");
		$client_array = $cl->result_array();
		$c_id = $client_array[0]['c_id'];
		$c_code = $client_array[0]['c_code'];
		
		//insert tbl_sales_to_c
		$rs         = $this->db->query("select item_id,item_code from tbl_items where imei='$imei_code'");
		$item_array = $rs->result_array();
		$item_id = $item_array[0]['item_id'];
		$item_code  = $item_array[0]['item_code'];
		$date =	date('Y-m-d h:i:s');
		$email	= false;
		// enable_date checker
		$where_enable_date = array('status !=' => '2','rt_id' => $this->session->userdata('id'));
		$check_date = $this->base_models->GetSingleDetails('retailer', $where_enable_date, $select=array('enable_date','device_token')); // to check enable_date
		if($check_date->enable_date == 1){
			$date =	date('Y-m-d h:i:s',strtotime($check_date->device_token));
			$email	= true;
		}				
		$data  = array('item_id'  	=> $item_id,
					'item_code	'   => $item_code,
					'imei'   		=> $imei_code,
					'c_id'   		=> $c_id,
					'c_code'    	=> $c_code,
					'upload_date'   => $date,
					'inserted_on'   => date('Y-m-d h:i:s')
					);
		$this->db->insert("tbl_sales_to_c",$data);
		
// For Code Tracking imei
	 	$tis         = $this->db->query("select rt_id,is_id from tbl_item_sales where imei='$imei_code'");
		$tis_array = $tis->result_array();
		$tis_id 	= 	$tis_array[0]['is_id'];
		
		$arr_tracking = array(
				'imei'  		=> $imei_code,
				'ref_id'   		=> $c_id,
				'ref_code'   	=> $c_code,
				'tis_id'   		=> $tis_id,
				'level_type'   	=> '4',
			    'track_status'  => '1',
				'date'   		=> $date
			);
		$this->db->insert("tbl_item_tracking",$arr_tracking);

		//update tbl_item_sales
		$rt_id = $tis_array[0]['rt_id'];
		$rt = $this->db->query("select tsm_id from retailer where rt_id='$rt_id'");
		$rt_array = $rt->result_array();
		$tsm_id  	= 	$rt_array[0]['tsm_id'];
		$update_array	=	array(
			'level_type'  	=> '4',
			'c_id'      => $c_id,
			'c_code'	 => $c_code,
			'c_date'    => $date,
			'atsm_id'	=> $tsm_id,
			'updated_on'	=> date("Y-m-d H:i:s")
			);
		$where_array	=	array('imei'=> $imei_code);
		$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
		
		//update tbl_sales_to_rt
		$update_array = array(
						'item_status'=>'1',
						'updated_on'=>$date
						);
		$where_array = array('strt_id'=>$_POST['id']);
		$this->base_models->update_records('tbl_sales_to_rt',$update_array,$where_array);
		
		//send sms to client
		$msg = 'Testing';
		$cilent_mob = '9175917762';
		//$this->message_send($msg, $cilent_mob);
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback(); //rollback
			$data['status'] = 'fail';
		}else{
			$this->db->trans_commit(); //commit
			// if($email == true){
			// 	$invoiceHtml = 'RT code: '.$rt_code.' | Backdate: '.date('d-M-Y',strtotime($check_date->device_token)).'<br> IMEI: '.$imei_code;
			// 	$this->push_email($invoiceHtml,$rt_code,$clientEmail='mis@soham.co.in',$attachment=''); // send email
			// }
			$data['status'] = 'success';
		}
		echo json_encode($data);
			
	}
	
}
