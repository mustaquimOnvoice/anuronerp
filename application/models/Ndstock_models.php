<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class ndstock_models extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	
	//get all users stock count
	public function get_all_stock_count($imei = null){
		$this->db->select('imei')
				->from('tbl_item_sales')
				->where_not_in('level_type', array('4','0'))
				->where(array('nd_id'=> $this->session->userdata('id')));
			if (isset ( $imei )){
				$this->db->where(array('imei'=>$imei));
			}
		$res = $this->db->get();
		return	$res->num_rows();
	}
	
	//get all users stock
	public function get_all_stock($imei = null, $limit = null, $start = null){
		$select = "tis.item_code, tis.imei, tis.level_type, 
					(CASE
					 WHEN tis.level_type = '0' THEN 'Anuron'
					 WHEN tis.level_type = '1' THEN (select nd_code from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
					 WHEN tis.level_type = '2' THEN (select d_code from distributor where d_id = tis.d_id Order by nd_id desc limit 1)
					 WHEN tis.level_type = '3' THEN (select rt_code from retailer where rt_id = tis.rt_id Order by nd_id desc limit 1)
					 ELSE 0
					END) as code,
					(CASE
					 WHEN tis.level_type = '0' THEN 'Anuron'
					 WHEN tis.level_type = '1' THEN (select firmname from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
					 WHEN tis.level_type = '2' THEN (select firmname from distributor where d_id = tis.d_id Order by d_id desc limit 1)
					 WHEN tis.level_type = '3' THEN (select firmname from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1)
					 ELSE 0
					END) as firmname,
					(CASE
					 WHEN tis.level_type = '0' THEN 'Anuron'
					 WHEN tis.level_type = '1' THEN 'National Distributor'
					 WHEN tis.level_type = '2' THEN 'Distributor'
					 WHEN tis.level_type = '3' THEN 'Retailer'
					 ELSE 0
					END) as level,
                    (CASE
					 WHEN tis.level_type = '0' THEN (select tis.inserted_on)
					 WHEN tis.level_type = '1' THEN (select tis.nd_date)
					 WHEN tis.level_type = '2' THEN (select tis.d_date)
					 WHEN tis.level_type = '3' THEN (select tis.rt_date)
					 ELSE 0
					END) as stock_date
					";
		$this->db->select($select,FALSE)
				->from('tbl_item_sales as tis')
				->where_not_in('level_type', array('4','0'))
				->where(array('nd_id'=> $this->session->userdata('id')));
		if(isset ( $imei )){
			$this->db->where(array('tis.imei'=> $imei));
		}
			$this->db->order_by("tis.level_type");
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		return $res->result_array();
	}
	
	
	// getting all Item values
	function GetAllItemValues($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		 $this->db->order_by('item_id', 'DESC');
		$querys = $this->db->get ();
		return $querys->result_array ();
	}

		function get_users($select = '*', $id = '', $searchText = '', $page='', $segment='')
    {
        $this->db->select($select);
        $this->db->from('wwc_admin');
        if(!empty($searchText)) {
            $likeCriteria = "(username  LIKE '%".$searchText."%'
                            OR  email  LIKE '%".$searchText."%'
                            OR  contact  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if(!empty($id)) {
			$this->db->where(array('id'=>$id));
		}
        $this->db->where('status !=', 2);
        // $this->db->order_by('id', 'DESC');
		if(!empty($page)) {
			$this->db->limit($page, $segment);
		}
        $query = $this->db->get();
        
		if(!empty($id)) {
			$result = $query->row(); 
		}else{
			$result = $query->result();
		}
        return $result;
    }

}
?>