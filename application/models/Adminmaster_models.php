<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class adminmaster_models extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	
	public function get_national_distributor($select,$table,$wherecondition = NULL,$orderby,$limit, $start) {    
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		if(!empty($orderby))
			$this->db->order_by("$orderby", 'DESC');
		$this->db->limit( $limit, $start );
		$query = $this->db->get( $table );
		return $query->result_array();
	}
   
	public function distributor_list($select,$table,$wherecondition = NULL,$orderby,$limit, $start) {  
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		if(!empty($orderby))
			$this->db->order_by("$orderby", 'DESC');
		$this->db->limit( $limit, $start );
		$query = $this->db->get( $table );
		return $query->result_array();
	}
   
	public function atsm_list($select,$table,$wherecondition = NULL,$orderby,$limit = NULL, $start = NULL) {    
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		if(!empty($orderby))
			$this->db->order_by("$orderby", 'DESC');
		if (isset ( $limit ))
			$this->db->limit( $limit, $start );
		$query = $this->db->get( $table );
		return $query->result_array();
	}
   
	public function retailer_list($select,$table,$wherecondition = NULL,$orderby,$limit = NULL, $start = NULL) {    
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		if(!empty($orderby))
			$this->db->order_by("$orderby", 'DESC');
		if (isset ( $limit ))
			$this->db->limit( $limit, $start );
		$query = $this->db->get( $table );
		return $query->result_array();
	}
}
?>