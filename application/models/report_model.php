<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class report_model extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	// getting all Item values
	function GetAllItemValues($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		 $this->db->order_by('nd_id', 'DESC');
		$querys = $this->db->get ();
		return $querys->result_array ();
	}
	// getting all Item values
	function get_retailer($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		 $this->db->order_by('rt_id', 'DESC');
		$querys = $this->db->get ();
		return $querys->result_array ();
	}
		function GetAllareaValues($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		$querys = $this->db->get ();
		return $querys->result_array ();
	}

		function get_users($select = '*', $id = '', $searchText = '', $page='', $segment='')
    {
        $this->db->select($select);
        $this->db->from('wwc_admin');
        if(!empty($searchText)) {
            $likeCriteria = "(username  LIKE '%".$searchText."%'
                            OR  email  LIKE '%".$searchText."%'
                            OR  contact  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if(!empty($id)) {
			$this->db->where(array('id'=>$id));
		}
        $this->db->where('status !=', 2);
        // $this->db->order_by('id', 'DESC');
		if(!empty($page)) {
			$this->db->limit($page, $segment);
		}
        $query = $this->db->get();
        
		if(!empty($id)) {
			$result = $query->row(); 
		}else{
			$result = $query->result();
		}
        return $result;
    }

    public function get_report_count($status,$fdate, $todate){
		$this->db->select('imei')
				->from('tbl_item_sales');
				switch($status){
					case '1' :
						$this->db->where(array('level_type !='=> '0'))
						->where(array('status'=> '1'))
						->where(array('nd_date  >='=> "$fdate"))
						->where(array('nd_date  <='=> "$todate"));	
									
						break;
					case '2' :
						$this->db->where_not_in('level_type', array('0','1'))
						->where(array('status'=> '1'))
						->where(array('d_date  >='=> "$fdate"))
						->where(array('d_date  <='=> "$todate"));

						break;
					case '3' :
						$this->db->where_in('level_type', array('3','4'))
						->where(array('status'=> '1'))
						->where(array('rt_date  >='=> "$fdate"))
						->where(array('rt_date  <='=> "$todate"));
						break;
					case '4' :
						$this->db->where_in('level_type', array('4'))
						->where(array('status'=> '1'))
						->where(array('c_date  >='=> "$fdate"))
						->where(array('c_date  <='=> "$todate"));
						break;
				}
			
		$res = $this->db->get();
		return	$res->num_rows();
	}
	
	//get all users stock
	public function get_report_list($status = null,$fdate,$todate, $limit = null, $start = null){

		switch($status){
			case '1' :
					// Intermediary query
					$select = "tis.item_code, tis.imei, tis.level_type, tis.c_id,
							'Anuron' as fromcode, 
							'Anuron' as fromfirmname, 
							(select nd_code from ndistributor where nd_id = tis.nd_id limit 1) as tocode, 
							(select firmname from ndistributor where nd_id = tis.nd_id limit 1) as tofirmname, 
							(select city_name from area where city_id = tis.nd_city_id limit 1) as cityname, 
							(select state_name from area where state_id = tis.nd_state_id limit 1) as state_name, 
							(select tis.nd_date) as sales_date";						
					break;
			case '2' :
					// Primary query
					$select = "tis.item_code, tis.imei, tis.level_type, tis.c_id,
							(select nd_code from ndistributor where nd_id = tis.nd_id limit 1) as fromcode, 
							(select firmname from ndistributor where nd_id = tis.nd_id limit 1) as fromfirmname, 
							(select d_code from distributor where d_id = tis.d_id limit 1) as tocode, 
							(select firmname from distributor where d_id = tis.d_id limit 1) as tofirmname, 
							(select city_name from area where city_id = tis.d_city_id limit 1) as cityname, 
							(select state_name from area where state_id = tis.nd_state_id limit 1) as state_name, 
							(select tis.d_date) as sales_date";						
					break;
			case '3' :
					// Secondary query
					$select = "tis.item_code, tis.imei, tis.level_type, tis.c_id,
							(select d_code from distributor where d_id = tis.d_id limit 1) as fromcode, 
							(select firmname from distributor where d_id = tis.d_id limit 1) as fromfirmname,
							(select rt_code from retailer where rt_id = tis.rt_id limit 1) as tocode, 
							(select firmname from retailer where rt_id = tis.rt_id limit 1) as tofirmname,
							tis.atsm_id as atsmid,
							(select atsm_code from atsm where atsm_id = atsmid limit 1) as atsmcode,
							(select fname from atsm where atsm_id = atsmid limit 1) as atsmfname,
							(select lname from atsm where atsm_id = atsmid limit 1) as atsmlname,
							(select level_type from atsm where atsm_id = atsmid limit 1) as atsmlevel,
							(select upline_id from atsm where atsm_id = atsmid limit 1) as uplineid,
							(select fname from atsm where atsm_id = uplineid limit 1) as asmfname,
							(select lname from atsm where atsm_id = uplineid limit 1) as asmlname,
							(select nd_code from ndistributor where nd_id = tis.nd_id limit 1) as ndcode,
							(select firmname from ndistributor where nd_id = tis.nd_id limit 1) as ndfirmname,
							(select city_name from area where city_id = tis.d_city_id limit 1) as cityname, 
							(select state_name from area where state_id = tis.nd_state_id limit 1) as state_name, 
							(select tis.rt_date) as sales_date";						
					break;
			case '4' :
					// Activation query
					$select = "tis.item_code, tis.imei, tis.level_type, tis.c_id,
							(select rt_code from retailer where rt_id = tis.rt_id limit 1) as fromcode, 
							(select firmname from retailer where rt_id = tis.rt_id limit 1) as fromfirmname,
							(select fname from client where c_id = tis.c_id limit 1) as tocode, 
							(select contact from client where c_id = tis.c_id limit 1) as tofirmname,
							tis.atsm_id as atsmid,
							(select atsm_code from atsm where atsm_id = atsmid limit 1) as atsmcode,
							(select fname from atsm where atsm_id = atsmid limit 1) as atsmfname,
							(select lname from atsm where atsm_id = atsmid limit 1) as atsmlname,
							(select level_type from atsm where atsm_id = atsmid limit 1) as atsmlevel,
							(select upline_id from atsm where atsm_id = atsmid limit 1) as uplineid,
							(select fname from atsm where atsm_id = uplineid limit 1) as asmfname,
							(select lname from atsm where atsm_id = uplineid limit 1) as asmlname,
							(select nd_code from ndistributor where nd_id = tis.nd_id limit 1) as ndcode,
							(select firmname from ndistributor where nd_id = tis.nd_id limit 1) as ndfirmname,
							(select d_code from distributor where d_id = tis.d_id limit 1) as dcode,
							(select firmname from distributor where d_id = tis.d_id limit 1) as dfirmname,
							(select city_name from area where city_id = tis.d_city_id limit 1) as cityname, 
							(select state_name from area where state_id = tis.nd_state_id limit 1) as state_name, 
							(select tis.c_date) as sales_date";
					break;
		}
		
		// $select = "tis.item_code, tis.imei, tis.level_type, tis.c_id, (CASE
		// 			 WHEN '".$status."' = '1' THEN 'Anuron'
		// 			 WHEN '".$status."' = '2' THEN (select nd_code from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
		// 			 WHEN '".$status."' = '3' THEN (select d_code from distributor where d_id = tis.d_id Order by d_id desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select rt_code from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1)
		// 			 ELSE 0
		// 			END) as fromcode, 
        //             (CASE
		// 			 WHEN '".$status."' = '1' THEN 'Anuron'
		// 			 WHEN '".$status."' = '2' THEN (select firmname from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
		// 			 WHEN '".$status."' = '3' THEN (select firmname from distributor where d_id = tis.d_id Order by d_id desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select firmname from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1)
		// 			 ELSE 0
		// 			END) as fromfirmname,
        //             (CASE
		// 			 WHEN '".$status."' = '1' THEN (select nd_code from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
		// 			 WHEN '".$status."' = '2' THEN (select d_code from distributor where d_id = tis.d_id Order by d_id desc limit 1)
		// 			 WHEN '".$status."' = '3' THEN (select rt_code from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select fname from client where c_id = tis.c_id Order by c_id desc limit 1)
		// 			 ELSE 0
		// 			END) as tocode, 
        //             (CASE
		// 			 WHEN '".$status."' = '1' THEN (select firmname from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
		// 			 WHEN '".$status."' = '2' THEN (select firmname from distributor where d_id = tis.d_id Order by d_id desc limit 1)
		// 			 WHEN '".$status."' = '3' THEN (select firmname from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select contact from client where c_id = tis.c_id Order by c_id desc limit 1)
		// 			 ELSE 0
		// 			END) as tofirmname,
		// 			(CASE
		// 			 WHEN '".$status."' = '3' THEN (select tsm_id from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select tsm_id from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1)
		// 			 ELSE 0
		// 			END) as atsmid,
        //             (CASE
		// 			 WHEN '".$status."' = '3' THEN (select atsm_code from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select atsm_code from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
		// 			 ELSE 0
		// 			END) as atsmcode,
		//				(CASE
		// 			 WHEN '".$status."' = '3' THEN (select fname from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select fname from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
		// 			 ELSE 0
		// 			END) as atsmfname,
		// 			(CASE
		// 			 WHEN '".$status."' = '3' THEN (select lname from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select lname from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
		// 			 ELSE 0
		// 			END) as atsmlname,
		// 			(CASE
		// 			 WHEN '".$status."' = '3' THEN (select level_type from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select level_type from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
		// 			 ELSE 0
		// 			END) as atsmlevel, 
		// 			(CASE
		// 			 WHEN '".$status."' = '3' THEN (select upline_id from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select upline_id from atsm where atsm_id = atsmid Order by atsmid desc limit 1)
		// 			 ELSE 0
		// 			END) uplineid,
		// 			(CASE
		// 			 WHEN '".$status."' = '3' THEN (select fname from atsm where atsm_id = uplineid Order by atsmid desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select fname from atsm where atsm_id = uplineid Order by atsmid desc limit 1)
		// 			 ELSE 0
		// 			END) asmfname,
		// 			(CASE
		// 			 WHEN '".$status."' = '3' THEN (select lname from atsm where atsm_id = uplineid Order by atsmid desc limit 1)
        //           WHEN '".$status."' = '4' THEN (select lname from atsm where atsm_id = uplineid Order by atsmid desc limit 1)
		// 			 ELSE 0
		// 			END) asmlname,
		// 			(CASE
		// 			 WHEN '".$status."' = '3' THEN (select nd_code from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
		// 			 WHEN '".$status."' = '4' THEN (select nd_code from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
        //              ELSE 0
		// 			END) as ndcode,
		// 			(CASE
		// 			 WHEN '".$status."' = '3' THEN (select firmname from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
		// 			 WHEN '".$status."' = '4' THEN (select firmname from ndistributor where nd_id = tis.nd_id Order by nd_id desc limit 1)
        //              ELSE 0
		// 			END) as ndfirmname,
		// 			(CASE
		// 			 WHEN '".$status."' = '4' THEN (select d_code from distributor where d_id = tis.d_id Order by d_id desc limit 1)
        //              ELSE 0
		// 			END) as dcode,
		// 			(CASE
		// 			 WHEN '".$status."' = '4' THEN (select firmname from distributor where d_id = tis.d_id Order by d_id desc limit 1)
        //              ELSE 0
		// 			END) as dfirmname,
        //             (CASE
		// 			 WHEN '".$status."' = '1' THEN (select city_name from area where city_id = tis.nd_city_id Order by city_id desc limit 1)
		// 			 WHEN '".$status."' = '2' THEN (select city_name from area where city_id = tis.d_city_id Order by city_id desc limit 1)
		// 			 WHEN '".$status."' = '3' THEN (select city_name from area where city_id = tis.d_city_id Order by city_id desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select city_name from area where city_id = tis.d_city_id Order by city_id desc limit 1)
		// 			 ELSE 0
		// 			END) as cityname,
        //             (CASE
		// 			 WHEN '".$status."' = '1' THEN (select state_name from area where state_id = tis.nd_state_id Order by state_id desc limit 1)
		// 			 WHEN '".$status."' = '2' THEN (select state_name from area where state_id = tis.nd_state_id Order by state_id desc limit 1)
		// 			 WHEN '".$status."' = '3' THEN (select state_name from area where state_id = tis.nd_state_id Order by state_id desc limit 1)
        //              WHEN '".$status."' = '4' THEN (select state_name from area where state_id = tis.nd_state_id Order by state_id desc limit 1)
		// 			 ELSE 0
		// 			END) as state_name,
        //             (CASE
		// 			 WHEN '".$status."' = '1' THEN (select tis.nd_date)
		// 			 WHEN '".$status."' = '2' THEN (select tis.d_date)
		// 			 WHEN '".$status."' = '3' THEN (select tis.rt_date)
        //              WHEN '".$status."' = '4' THEN (select tis.c_date)
		// 			 ELSE 0
		// 			END) as sales_date
		// 			";
					
		$this->db->select($select,FALSE)
				->from('tbl_item_sales as tis');
				switch($status){
					case '1' :
						$this->db->where(array('level_type !='=> '0','status'=> '1'))
						->where(array('tis.nd_date  >='=> "$fdate"))
						->where(array('tis.nd_date  <='=> "$todate"));						
						break;
					case '2' :
						$this->db->where_not_in('level_type', array('0','1'))
						->where(array('status'=> '1'))
						->where(array('tis.d_date  >='=> "$fdate"))
						->where(array('tis.d_date  <='=> "$todate"));
						break;
					case '3' :
						$this->db->where_in('level_type', array('3','4'))
						->where(array('status'=> '1'))
						->where(array('tis.rt_date  >='=> "$fdate"))
						->where(array('tis.rt_date  <='=> "$todate"));
						break;
					case '4' :
						$this->db->where_in('level_type', array('4'))
						->where(array('status'=> '1'))
						->where(array('tis.c_date  >='=> "$fdate"))
						->where(array('tis.c_date  <='=> "$todate"));
						break;
				}
			$this->db->order_by("tis.level_type");
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		return $res->result_array();
	}



       function get_report_excel_details_nd($where = '',$select=''){

		$data = $this->db->select($select)
					->from('tbl_sales_to_nd as tsnd')
					->join('tbl_item_sales as tis','ON tis.imei = tsnd.imei','Left')
					->join('area as ar','ON tis.nd_city_id = ar.city_id','Left')
					->join('ndistributor as ndist','ON tis.nd_code = ndist.nd_code','Left')
					->where($where)
					->group_by('imei');
				//	print_r($this->db->last_query());   die();
		return	$this->db->get()->result_array();
	}

    function get_report_details_nd($where = '',$select='',$orderby,$limit, $start){

			$this->db->select($select)
			->from('tbl_sales_to_nd as tsnd')
			->join('tbl_item_sales as tis','ON tis.imei = tsnd.imei','Left')
			->join('area as ar','ON tis.nd_city_id = ar.city_id','Left')
			->join('ndistributor as ndist','ON tis.nd_code = ndist.nd_code','Left')
			->where($where)
			->group_by('imei');
			if(!empty($orderby))
				$this->db->order_by("$orderby", 'DESC');
			$this->db->limit( $limit, $start );
			$data = $this->db->get();
			//$this->db->get()->result_array();
			//print_r($this->db->last_query());   die();
		return	$data->result_array();
	}
    
    function get_report_details_nd_count($where = '',$select=''){

		$data = $this->db->select($select)
					->from('tbl_sales_to_nd as tsnd')
					->join('tbl_item_sales as tis','ON tis.imei = tsnd.imei','Left')
					->join('area as ar','ON tis.nd_state_id = ar.state_id','Left')
					->join('ndistributor as ndist','ON tis.nd_code = ndist.nd_code','Left')
					->where($where)
					->group_by('imei')
					->get()->num_rows();
					/*print_r($this->db->last_query());   die();*/
		return	$data;
	}
 
  // for distributer
	 function get_report_excel_details_d($where = '',$select=''){

		$data = $this->db->select($select)
					->from('tbl_sales_to_d as tsd')
					->join('tbl_item_sales as tis','ON tis.imei = tsd.imei','Left')
					->join('area as ar','ON tis.d_city_id = ar.city_id','Left')
					->join('distributor as ddist','ON tis.d_code = ddist.d_code','Left')
					->where($where)
					->group_by('imei');
				//	print_r($this->db->last_query());   die();
		return	$this->db->get()->result_array();
	}
	function get_report_details_d($where = '',$select='',$orderby,$limit, $start){

			$this->db->select($select)
			->from('tbl_sales_to_d as tsd')
			->join('tbl_item_sales as tis','ON tis.imei = tsd.imei','Left')
			->join('area as ar','ON tis.d_city_id = ar.city_id','Left')
			->join('distributor as ddist','ON tis.d_code = ddist.d_code','Left')
			->where($where)
			->group_by('imei');
			if(!empty($orderby))
				$this->db->order_by("$orderby", 'DESC');
			$this->db->limit( $limit, $start );
			$data = $this->db->get();
			//$this->db->get()->result_array();
			//print_r($this->db->last_query());   die();
		return	$data->result_array();
	}
    
    function get_report_details_d_count($where = '',$select=''){

		$data = $this->db->select($select)
					->from('tbl_sales_to_d as tsd')
					->join('tbl_item_sales as tis','ON tis.imei = tsd.imei','Left')
					->join('area as ar','ON tis.nd_state_id = ar.state_id','Left')
					->join('distributor as ddist','ON tis.d_code = ddist.d_code','Left')
					->where($where)
					->group_by('imei')
					->get()->num_rows();
					/*print_r($this->db->last_query());   die();*/
		return	$data;
	}
	// end for distributer
	// retailer start
		 function get_report_excel_details_rt($where = '',$select=''){

		$data = $this->db->select($select)
					->from('tbl_sales_to_rt as tsrt')
					->join('tbl_item_sales as tis','ON tis.imei = tsrt.imei','Left')
					->join('area as ar','ON tis.d_city_id = ar.city_id','Left')
					->join('retailer as rt','ON tis.rt_code = rt.rt_code','Left')
					->where($where)
					->group_by('imei');
				//	print_r($this->db->last_query());   die();
		return	$this->db->get()->result_array();
	}
		function get_report_details_rt($where = '',$select='',$orderby,$limit, $start){

			$this->db->select($select)
			->from('tbl_item_sales as tis')
			->join('tbl_sales_to_rt as tsrt','ON tis.imei = tsrt.imei','Left')
			->where($where)
			->group_by('imei');
			if(!empty($orderby))
				$this->db->order_by("$orderby", 'DESC');
			$this->db->limit( $limit, $start );
			$data = $this->db->get();
			//$this->db->get()->result_array();
			//print_r($this->db->last_query());   die();
		return	$data->result_array();
	}

	  function get_report_details_rt_count($where = '',$select=''){

		$data = $this->db->select($select)
					->from('tbl_item_sales as tis')
					->join('tbl_sales_to_rt as tsrt','ON tis.imei = tsrt.imei','Left')
					->where($where)
					->group_by('imei')
					->get()->num_rows();
				
		return	$data;
	}
	// end for retailer

	// Customer start
		 function get_report_excel_details_c($where = '',$select=''){

		$data = $this->db->select($select)
					->from('tbl_sales_to_c as tsc')
					->join('tbl_item_sales as tis','ON tis.imei = tsc.imei','Left')
					->join('area as ar','ON tis.d_city_id = ar.city_id','Left')
					->join('client as c','ON tis.c_code = c.c_code','Left')
					->where($where)
					->group_by('imei');
				//	print_r($this->db->last_query());   die();
		return	$this->db->get()->result_array();
	}
		function get_report_details_c($where = '',$select='',$orderby,$limit, $start){

			$this->db->select($select)
			->from('tbl_sales_to_c as tsc')
			->join('tbl_item_sales as tis','ON tis.imei = tsc.imei','Left')
			->join('area as ar','ON tis.d_city_id = ar.city_id','Left')
			->join('client as c','ON tis.c_code = c.c_code','Left')
			->where($where)
			->group_by('imei');
			if(!empty($orderby))
				$this->db->order_by("$orderby", 'DESC');
			$this->db->limit( $limit, $start );
			$data = $this->db->get();
			//$this->db->get()->result_array();
			//print_r($this->db->last_query());   die();
		return	$data->result_array();
	}

	  function get_report_details_c_count($where = '',$select=''){

		$data = $this->db->select($select)
					->from('tbl_sales_to_c as tsc')
					->join('tbl_item_sales as tis','ON tis.imei = tsc.imei','Left')
					->join('area as ar','ON tis.nd_state_id = ar.state_id','Left')
					->join('client as c','ON tis.c_code = c.c_code','Left')
					->where($where)
					->group_by('imei')
					->get()->num_rows();
				
		return	$data;
	}
	// end for Customer
	public function get_count($select,$table,$wherecondition = NULL) {
		$this->db->select ( $select );
			  if (isset ( $wherecondition ))
		$this->db->where ( $wherecondition );
  		return $this->db->get( $table )->num_rows();
    // $this->db->last_query(); die;
	}

   public function get_pagination($table,$wherecondition = NULL,$orderby,$limit, $start) {       
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		if(!empty($orderby))
			$this->db->order_by("$orderby", 'DESC');
		$this->db->limit( $limit, $start );
		$query = $this->db->get( $table );
		return $query->result_array();
   }

	public function get_all_stock_count($imei = null){
		$this->db->select('imei')
				->from('tbl_item_sales')
				->where(array('level_type !='=> '4'));
			if (isset ( $imei )){
				$this->db->where(array('imei'=>$imei));
			}
		$res = $this->db->get();
		return	$res->num_rows();
	}	
	
	public function get_all_stock($imei){
		$select = "item_id, item_code, imei, level_type, nd_id,nd_code, nd_date,(select nd.firmname from ndistributor nd where nd.nd_id = tis.nd_id Order by nd.nd_id desc limit 1) as nd_firmname,d_id,d_code, d_date, (select d.firmname from distributor d where d.d_id = tis.d_id Order by d.d_id desc limit 1) as d_firmname, rt_id, rt_code, rt_date, (select rt.firmname from retailer rt where rt.rt_id = tis.rt_id Order by rt.rt_id desc limit 1) as rt_firmname, c_id, c_code, c_date, (select c.fname from client c where c.c_id = tis.c_id Order by c.c_id desc limit 1) as c_firmname,(select c.contact from client c where c.c_id = tis.c_id Order by c.c_id desc limit 1) as c_mno";

		$this->db->select($select,FALSE)
				->from('tbl_item_sales as tis');
			
		if(isset ( $imei )){
			$this->db->where(array('tis.imei'=> $imei));
		}
		$res = $this->db->get();
		return $res->result_array();
	}

	public function get_all_imei_history_count($imei = null){
		$this->db->select('imei')
				->from('tbl_item_sales')
				->where(array('level_type !='=> '4'));
			if (isset ( $imei )){
				$this->db->where(array('imei'=>$imei));
			}
		$this->db->order_by("tit_id", 'ASC');
		$res = $this->db->get();
		return	$res->num_rows();
	}	
	
	public function get_all_imei_history($imei){
		$select = "tit.imei, tit.level_type,tit.track_status,tit.ref_id,tit.date, (CASE
					 WHEN level_type = '0' THEN 'Anuron'
					 WHEN level_type = '1' THEN (select nd_code from ndistributor where nd_id = tit.ref_id Order by nd_id desc limit 1)
					 WHEN level_type = '2' THEN (select d_code from distributor where d_id = tit.ref_id Order by d_id desc limit 1)
                     WHEN level_type = '3' THEN (select rt_code from retailer where rt_id = tit.ref_id Order by rt_id desc limit 1)
                     WHEN level_type = '4' THEN (select c_code from client where c_id = tit.ref_id Order by c_id desc limit 1)
					 ELSE 0
					END) as ref_code, (CASE
					 WHEN level_type = '0' THEN 'Anuron'
					 WHEN level_type = '1' THEN (select firmname from ndistributor where nd_id = tit.ref_id Order by nd_id desc limit 1)
					 WHEN level_type = '2' THEN (select firmname from distributor where d_id = tit.ref_id Order by d_id desc limit 1)
                     WHEN level_type = '3' THEN (select firmname from  retailer where rt_id = tit.ref_id Order by rt_id desc limit 1)
                     WHEN level_type = '4' THEN (select fname from client where c_id = tit.ref_id Order by c_id desc limit 1)
					 ELSE 0
					END) as ref_name,
                   ";

		$this->db->select($select,FALSE)
				->from('tbl_item_tracking as tit');
		
		if(isset ( $imei )){
			$this->db->where(array('tit.imei'=> $imei));
		}
		$this->db->order_by("tit_id", 'ASC');
		$res = $this->db->get();
		 	
		return $res->result_array();

	}

}
?>