<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class report_models extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	
	function get_citywise_data($where){
		$condition = '';
		if($where != ''){
			$condition = "WHERE $where";
		}
		return $this->db->query(
						"SELECT tc.name as name, tb.city_id as cityid, sum(tb.city_rate) as rate
						FROM tbl_city as tc
						LEFT JOIN tbl_booking as tb
						ON tc.id = tb.city_id
						LEFT JOIN tbl_adv as ta
						ON tb.adv_id = ta.id
						$condition
						Group By tb.city_id"
						)->result_array();
	}
	
	function get_catwise_data($where){
		$condition = '';
		if($where != ''){
			$condition = "WHERE $where";
		}
		return $this->db->query(
						"SELECT bc.bname as name, sum(ta.net_amt) as rate
						FROM `business_cat` as bc
						LEFT JOIN tbl_client as tc
						ON bc.id = tc.bcat
						LEFT JOIN tbl_adv as ta
						ON tc.id = ta.client_id
						$condition
						GROUP BY tc.bcat
						ORDER BY bc.bname ASC"
						)->result_array();
	}
	
	function get_invoice_details($id = ''){
		$select = array('td.id','td.user_id','td.client_id','td.city_id','td.ro_no','td.schedule_type','td.time','td.date','td.amt','td.net_amt','td.net_amt','td.invoice_no','td.invoice_date','td.supplier_ref',
						'wa.fname','wa.lname','tc.business_name','tc.address');		
		$where = array('td.id' => $id);
		$data = $this->db->select($select)
					->from('tbl_adv as td')
					->join('wwc_admin as wa','ON td.user_id = wa.id','Left')
					->join('tbl_client as tc','ON td.client_id = tc.id','Left')
					->where($where)
					->get()->row();
		return	$data;
	}

}
?>