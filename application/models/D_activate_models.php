<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class d_activate_models extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	// getting all Item values
	function GetAllItemValues($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		 $this->db->order_by('rt_id', 'DESC');
		$querys = $this->db->get ();
		return $querys->result_array ();
	}

		function get_users($select = '*', $id = '', $searchText = '', $page='', $segment='')
    {
        $this->db->select($select);
        $this->db->from('wwc_admin');
        if(!empty($searchText)) {
            $likeCriteria = "(username  LIKE '%".$searchText."%'
                            OR  email  LIKE '%".$searchText."%'
                            OR  contact  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if(!empty($id)) {
			$this->db->where(array('id'=>$id));
		}
        $this->db->where('status !=', 2);
        // $this->db->order_by('id', 'DESC');
		if(!empty($page)) {
			$this->db->limit($page, $segment);
		}
        $query = $this->db->get();
        
		if(!empty($id)) {
			$result = $query->row(); 
		}else{
			$result = $query->result();
		}
        return $result;
    }
/*
    function get_single_imei_rt($where = '',$select='',$orderby,$limit, $start){

			$this->db->select($select)
			->from('tbl_sales_to_rt as tsrt')
			->join('tbl_item_sales as tis','ON tis.imei = tsrt.imei','Left')
			->join('area as ar','ON tis.d_city_id = ar.city_id','Left')
			->join('retailer as rt','ON tis.rt_code = rt.rt_code','Left')
			->where($where)
			->group_by('imei');
			if(!empty($orderby))
				$this->db->order_by("$orderby", 'DESC');
			$this->db->limit( $limit, $start );
			$data = $this->db->get();
			//$this->db->get()->result_array();
			//print_r($this->db->last_query());   die();
		return	$data->result_array();
	}

	  function get_single_imei_rt_count($where = '',$select=''){

		$data = $this->db->select($select)
					->from('tbl_sales_to_rt as tsrt')
					->join('tbl_item_sales as tis','ON tis.imei = tsrt.imei','Left')
					->join('area as ar','ON tis.nd_state_id = ar.state_id','Left')
					->join('retailer as rt','ON tis.rt_code = rt.rt_code','Left')
					->where($where)
					->group_by('imei')
					->get()->num_rows();
				
		return	$data;
	}*/
	//get all users stock
	public function get_single_imei_rt($d_id,$limit, $start,$imei=NULL){
		$select = "tis.item_code, tis.imei, tis.rt_date, (select rt_code from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1) as rt_code, (select firmname from retailer where rt_id = tis.rt_id Order by rt_id desc limit 1) as tofirmname";
		$where = "tis.d_id='$d_id' AND tsrt.item_status='0'";			
		$this->db->select($select,FALSE)
				->from('tbl_item_sales as tis')
				->join('tbl_sales_to_rt as tsrt','ON tis.imei = tsrt.imei','Left');
				$this->db->where($where);
			if($imei != NULL)	{
				$this->db->where(array('tis.imei'=>$imei ));	
			}
			$this->db->order_by("tis.level_type");
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		return $res->result_array();
	}
		public function get_single_imei_rt_cnt($d_id){
		$select = "tis.imei";
		$where = "tis.d_id='$d_id' AND tsrt.item_status='0'";			
		$this->db->select($select,FALSE)
				->from('tbl_item_sales as tis')
				->join('tbl_sales_to_rt as tsrt','ON tis.imei = tsrt.imei','Left');
				$this->db->where($where);
				
		return $res = $this->db->get()->num_rows(); 
	}
}
?>