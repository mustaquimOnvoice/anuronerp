<title>Search By IMEI</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
<script>
	var tblexport = true;
	console.log('tblexport: '+tblexport);
  console.log('CSS: assets/css/common/listing.css');
</script>
<!-- Listing CSS -->
<link href="<?php echo base_url();?>assets/css/common/listing.css"   rel="stylesheet">
</head>

<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
	<div class="row">
		<div class="white-box col-sm-12">
		  <form method="post" name="date-form" id="date-form" action="<?php echo site_url('Ndreport/sales_detail_report_history_sess'); ?>" data-toggle="validator">       
			<div class="col-sm-2">
				<input type="text" class="form-control" id="imei" name="imei" placeholder="IMEI" value="<?php @$imei = $select['imei']; echo ($imei == '') ? '' : $imei ; ?>" required>
			</div>
			
			<div class="col-sm-2">
			  <button type="submit" name="submit" value="filter" class="pull-left btn-xs btn-success">Search</button>       
			  <button type="submit" name="submit" value="createxls" class="pull-left btn-xs btn-primary"><i class="fa fa-file-excel-o"></i> Export Excel</button>
			</div>
		  </form>
		 <div class="col-sm-12">
		  	<br>
		  	<label>For Bulk IMEI Search By Excel <a href="<?php echo site_url('Ndreport/bulk_imei/');?>">Click Here</a></label>
		</div>
		</div>
		
	</div>      
	<div class="row">
		<div class="col-lg-12 col-xs-12">
		<div id ="resultMsg">
		</div>
			<?php if($this->session->flashdata('success')){	?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
			<?php } if($this->session->flashdata('error')){	?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('error') ?>
				</div>
			<?php }	?>
		</div>
	</div>
      <div class="row">
		<div class="col-sm-12">
          <div class="white-box">
            <div class="table-responsive">
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Sale Type</th>
                            <th>Date</th>
                            <th>Level Type</th>
                            <th>From Code</th>
                            <th>From Firmname</th>
                            <th>To Code</th>
                            <th>To Firmname</th>
                          	<th>IMEI</th>
                           
                        </tr>
                    </thead>
                 
                    <tbody>
                         
                        <?php $i = 1;
                        $from_code	=	"Anuron";
                        $from_name	=	"Anuron";
                       // print_r($results);
                        if(!empty($results)){
								foreach($results as $row){
									?>
									
										<tr>
											<td><?php echo $i++;?></td>
											<td><?php echo $track_status = ($row['track_status']== 1 ? "Sale" : "Sale Return");?></td>
											<td><?php echo date("d-M-y", strtotime($row['date']));?></td>
											<td><?php 
													switch ($row['level_type']){
														 case '0':
														 $type="Company";
														   break;
														  case '1':
														  $type="Intermediary";
														   break;
														  case '2':
														  $type="Primary";
														   break;
														 	case '3':
														 	$type="Secondary";
														   break;
														  case '4':
														  $type="Activation";
														   break;
													}
													echo $type;?>
														
											</td>
											<td><?php echo $from_code;?></td>
											<td><?php echo $from_name;?></td>
											<td><?php echo $from_code = $row['ref_code'];?></td>
											<td><?php echo $from_name = $row['ref_name'];?></td>
											<td><?php echo $row['imei'];?></td>
										</tr>
									
									
						<?php  } } ?>                    
                    </tbody>
                              
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/common/listing.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

</body>
</html>
