
<!DOCTYPE html>
<html lang="en">



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/images/favicon.png">
    <title>View User</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Menu CSS -->
    <link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url();?>assets/css/colors/default.css" id="theme" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
</head>

<body class="fix-sidebar">
    
    <div id="wrapper">
        <!-- Top Navigation -->
        <?php include 'header.php';?>
        <?php include 'navigation.php';?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">View User</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo site_url('Admin/dashboard/');?>">Dashboard</a></li>
							<li class="active"><a href="<?php echo site_url('Admin/view_user/');?>">View User</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
				
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <!--<h3 class="box-title m-b-0">Data Table</h3>
                            <p class="text-muted m-b-30">Data table example</p>-->
                            <div class="table-responsive">
							<h4 class="box-title m-b-0 text-center" style="color:#03a9f3;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('success');?> </h4>
							<h4 class="box-title m-b-0 text-center" style="color:#03a9f3;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('delete');?> </h4>
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Mobile Number</th>
                                            <th>Email</th>
                                            <th>Password</th>
                                            <th>Category</th>
                                            <th>Register Date</th>
                                            <th>Expire Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										$j=1;
										foreach($user_details as $rows)
										{
										?>
                                        <tr>
                                            <td><?php echo $j++;?></td>
                                            <td><?php echo $rows['name'];?></td>
                                            <td><?php echo $rows['mno'];?></td>
                                            <td style="text-transform:lowercase;"><?php echo $rows['email'];?></td>
                                            <td><?php echo $rows['password'];?></td>
                                            <td>
											<?php 
											$cat=explode(",",$rows['category_id']);
											$i=0;
												foreach($cat as $row)
													{
														$query=$this->db->query("select category_name from tbl_category where id=$row[$i]")->result_array();
														echo $query[0]['category_name'];
														echo " ,";
														echo "<br>";
													}
											?>
											</td>
                                            <td><?php echo $rows['reg_date'];?></td>
                                            <td><div class="label label-table label-danger"><?php echo $rows['exp_date'];?></div></td>
                                            <td><div class="<?php echo ($status=$rows['is_active']==0) ?  "label label-table label-success" : "label label-table label-danger" ?> "><?php $status=$rows['is_active'] ; echo ($status==0 ? 'Active':'Inactive');?></div></td>
                                            <td><div class="button-box">
												<div class="btn-group m-r-10">
												<button aria-expanded="false" data-toggle="dropdown" class="btn btn-info dropdown-toggle waves-effect waves-light" type="button">Action <span class="caret"></span></button>
												<ul role="menu" class="dropdown-menu">
													<li><a href="<?php echo site_url('Admin/edit_user/'.$rows['user_id'].'/');?>">Edit</a></li>
													<li><a href="<?php echo site_url('Admin/delete_user/'.$rows['user_id'].'/');?>" onclick="return confirm('Are You Sure Want to Delete <?php echo $rows['name'];?>')">Delete</a></li>
												</ul>
												</div>
											</td>
                                        </tr>
										<?php 
										}
										?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.row -->
                <!-- .right-sidebar -->
                <?php include 'right-sidebar.php';?>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <?php include 'footer.php';?>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    });
  });
    });
    $('#example23').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
  </script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
<script>
	setTimeout(function() 
		{
            $('#timeout').fadeToggle('slow');
		}, 3000);
	setTimeout(function() 
		{
            $('#timeout1').fadeToggle('slow');
		}, 3000);
			
</script>


</html>
