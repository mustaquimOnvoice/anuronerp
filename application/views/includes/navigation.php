<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Preloader --
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<!-- Preloader	
<div class="preloader">
	<svg class="circular" viewBox="25 25 50 50">
		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
	</svg>
</div> -->
<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse slimscrollsidebar">
		<ul class="nav" id="side-menu">
			<?php if($this->session->userdata('login_type') == 'Admin'){?>
				<!-- <li><a href="<?php // echo site_url('admin/dashboard/');?>">Dashboard</a></li> -->
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Master<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						 <li> <a href="<?php echo site_url('Adminmaster/add_nd/');?>">Add ND</a> </li>
						<li> <a href="<?php echo site_url('Adminmaster/national_distributor/');?>">ND Master</a> </li>
						<li> <a href="<?php echo site_url('Adminmaster/distributor/');?>">Distributor Master</a> </li>
						<li> <a href="<?php echo site_url('Adminmaster/distributor_list/');?>">Distributor List</a> </li>
						<li> <a href="<?php echo site_url('Adminmaster/retailer/');?>">Retailer Master</a> </li>
						<li> <a href="<?php echo site_url('Adminmaster/retailer_list/');?>">Retailer List</a> </li>
						<li> <a href="<?php echo site_url('Ndmaster/add_atsm/');?>">Add ASM/TSM</a> </li>
					<!--<li> <a href="<?php // echo site_url('Ndmaster/uploads_tsm_excel/');?>">Upload TSM</a> </li> -->
						<li> <a href="<?php echo site_url('Ndmaster/atsm/');?>">ASM/TSM List</a> </li>
						<li> <a href="<?php echo site_url('Adminmaster/retailer_tsm_mapping/');?>">Retailer TSM Mapping</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Item<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Adminitems/uploads_items/');?>">Upload Stock</a> </li>
						<li> <a href="<?php echo site_url('Adminitems/add_items/');?>">Add Item</a> </li>
						<li> <a href="<?php echo site_url('Adminitems/item_list/');?>">Items List</a> </li>
					<!--<li> <a href="<?php // echo site_url('Adminitems/pagination_item_list/');?>">Pagination Items List</a> </li>-->
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Sale<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						 <li> <a href="<?php echo site_url('Adminsales/sale_items/');?>">Sale Items</a> </li>
						<li> <a href="<?php echo site_url('Adminsales/sale_list/');?>">Sales List to ND</a> </li>
						<li> <a href="<?php echo site_url('Adminsales/sale_return/');?>">Sale Return</a> </li>
						<li> <a href="<?php echo site_url('Adminsales/faulty_return_list/');?>">Confirm DOA</a> </li>
						<li> <a href="<?php echo site_url('Adminsales/faulty_return_list_of_admin/');?>">DOA List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Reports<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<!--  <li> <a href="<?php //echo site_url('Adminsales/sale_items/');?>">Sale Items</a> </li>
						<li> <a href="<?php // echo site_url('admin/Admin/users/');?>">Search IMEI</a> </li> -->

						 <li> <a href="<?php echo site_url('Adminreport/report_list/');?>">Report List</a> </li>
						<!-- <li> <a href="<?php //echo site_url('Adminreport/sales_detail_report/');?>">Search IMEI</a> </li> -->
						<li> <a href="<?php echo site_url('Adminreport/sales_detail_report_history/');?>">Search IMEI Tracking</a> </li>
						<!-- <li> <a href="<?php //echo site_url('Adminreport/bulk_imei/');?>">Bulk IMEI Search</a> </li> -->
						<li> <a href="<?php echo site_url('Adminstock/admin/');?>">Stock</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">App<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Products/add_product/');?>">Add Product</a> </li>
						<li> <a href="<?php echo site_url('Products/product_list/');?>">Products List</a> </li>
						<li> <a href="<?php echo site_url('Products/add_scheme/');?>">Add Scheme</a> </li>
						<li> <a href="<?php echo site_url('Products/scheme_list/');?>">Scheme List</a> </li>
						<li> <a href="<?php echo site_url('Rtvisit/rt_visit_list/');?>">RT Visit List</a> </li>
						<li> <a href="<?php echo site_url('attendance/all');?>">TSM Attendance</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Backup<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Admin/backup');?>">Database</a> </li>
					</ul>
				</li>
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'National_distributor'){?>
				<!-- <li><a href="<?php // echo site_url('National_distributor/dashboard/');?>">Dashboard</a></li> -->
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Master<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Ndmaster/add_d/');?>">Add Distributor</a> </li>
						<li> <a href="<?php echo site_url('Ndmaster/distributor/');?>">Distributor List</a> </li>
						<li> <a href="<?php echo site_url('Ndmaster/retailer_list/');?>">Retailer List</a> </li>
					</ul>
				</li>
				<!--<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Stock<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php //echo site_url('Ndstock/stock_list/');?>">National Distributor</a> </li>
						<li> <a href="<?php //echo site_url('Ndsales/sale_list/');?>">Distributor's</a> </li>
						<li> <a href="<?php //echo site_url('Ndstock/rt_stock/');?>">Retailer's</a> </li>
					</ul>
				</li>-->
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Sales<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						 <li> <a href="<?php echo site_url('Ndsales/sale_to_d/');?>">Sales To Distributer</a> </li>	
						<li> <a href="<?php  echo site_url('Ndsales/sale_list/');?>">Sale List</a> </li>
						<li> <a href="<?php echo site_url('Ndsales/sale_return/');?>">Sale Return</a> </li>
						<li> <a href="<?php echo site_url('Ndsales/faulty_return_list/');?>">Confirm DOA</a> </li>
						<li> <a href="<?php echo site_url('Ndsales/faulty_return_list_of_nd/');?>">DOA List</a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Reports<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<!--<li> <a href="<?php //echo site_url('Adminsales/sale_items/');?>">Sale Items</a> </li>
						<li> <a href="<?php // echo site_url('admin/Admin/users/');?>">Search IMEI</a> </li> -->
						<li> <a href="<?php echo site_url('Ndreport/report_list/');?>">Report List</a> </li>
						<!-- <li> <a href="<?php // echo site_url('Ndreport/sales_detail_report/');?>">Search IMEI</a> </li> -->
						<li> <a href="<?php echo site_url('Ndreport/sales_detail_report_history/');?>">Search IMEI Tracking</a> </li>
						<li> <a href="<?php echo site_url('Ndstock/nd/');?>">Stock</a> </li>
					</ul>
				</li>
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'Distributor'){?>
				<!-- <li><a href="<?php // echo site_url('Distributor/dashboard/');?>">Dashboard</a></li> -->
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Master<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Dmaster/add_rt/');?>">Add Retailer</a> </li>
						<li> <a href="<?php echo site_url('Dmaster/retailer/');?>">Retailer List</a> </li>
					</ul>
				</li>
				<!--<li><a href="<?php //echo site_url('Dstock/stock_list/');?>">Stock</a></li>-->
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Sales<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Dsales/sale_to_rt/');?>">Sales To Retailer</a> </li>
					<!--	<li> <a href="<?php //echo site_url('Dsales/upload_excel_for_sale/');?>">Sales To Retailer(By Excel)</a> </li>-->
						<!--<li> <a href="<?php //echo site_url('Dsales/sale_list/');?>">Sale List</a> </li>-->
						<li> <a href="<?php echo site_url('Dsales/sale_return/');?>">Sale Return</a> </li>
						<li> <a href="<?php echo site_url('Dsales/faulty_return/');?>">Add DOA</a> </li>
						<li> <a href="<?php echo site_url('Dsales/faulty_return_list/');?>">Confirm DOA</a> </li>
						<li> <a href="<?php echo site_url('Dsales/faulty_return_list_of_d/');?>">DOA List</a> </li>
					<!--	<li> <a href="<?php //echo site_url('Dsales/sale_return_by_excel/');?>">Sale Return(By Excel)</a> </li>-->
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Activation<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li><a href="<?php echo site_url('Dactivate/activation/');?>">Activation</a></li>
						<li> <a href="<?php echo site_url('Dactivate/single_imei/');?>">Single Activation </a> </li>
					</ul>
				</li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Reports<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<!--<li> <a href="<?php //echo site_url('Adminsales/sale_items/');?>">Sale Items</a> </li>
						<li> <a href="<?php // echo site_url('admin/Admin/users/');?>">Search IMEI</a> </li> -->
						<li> <a href="<?php echo site_url('Dreport/report_list/');?>">Report List</a> </li>
						<!-- <li> <a href="<?php // echo site_url('Dreport/sales_detail_report/');?>">Search IMEI</a> </li> -->
							<li> <a href="<?php echo site_url('Dreport/sales_detail_report_history/');?>">Search IMEI Tracking</a> </li>
						<li> <a href="<?php echo site_url('Dstock/d/');?>">Stock</a> </li>
					</ul>
				</li>
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'Retailer'){?>
				<li><a href="<?php echo site_url('Retailer/stock_list');?>">Stock</a></li>				
				<li><a href="<?php echo site_url('Retailer/activated_list/');?>">Activation</a></li>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Sales<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Retailer/faulty_return/');?>">Add DOA</a> </li>
						<li> <a href="<?php echo site_url('Retailer/faulty_return_list/');?>">DOA List</a> </li>
					</ul>
				</li>
				<li><a href="<?php echo site_url('Rtreport/report_list/');?>">Report</a></li>
			<?php }?>
		</ul>
	</div>
</div>
<!-- Left navbar-header end -->