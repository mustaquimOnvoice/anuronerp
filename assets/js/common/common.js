//Flash message
function flashmessage(err,text){
	  switch(err){
		case 'error':
			icon = 'error';
			heading = 'Fail';
			break;
		case 'success':
			icon = 'success';
			heading = 'Success';
			break;
		case 'info':
			icon = 'info';
			heading = 'Info';
			break;
		default :
			icon = 'info';
			heading = 'Info';
			break;
	  }
	  
	  $.toast({
		heading: heading,
		text: text,
		position: 'top-right',
		loaderBg: '#ff6849',
		icon: icon,
		hideAfter: 3500,        
		stack: 6
	  })
}

// $(document).ready(function() {
	
// });